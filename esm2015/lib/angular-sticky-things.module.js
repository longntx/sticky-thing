/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { StickyThingDirective } from './sticky-thing.directive';
export class AngularStickyThingsModule {
}
AngularStickyThingsModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [
                    StickyThingDirective,
                ],
                exports: [
                    StickyThingDirective,
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1zdGlja3ktdGhpbmdzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B3MTFrL2FuZ3VsYXItc3RpY2t5LXRoaW5ncy8iLCJzb3VyY2VzIjpbImxpYi9hbmd1bGFyLXN0aWNreS10aGluZ3MubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLDBCQUEwQixDQUFDO0FBVzlELE1BQU0sT0FBTyx5QkFBeUI7OztZQVRyQyxRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsWUFBWSxFQUFFO29CQUNaLG9CQUFvQjtpQkFDckI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLG9CQUFvQjtpQkFDckI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtTdGlja3lUaGluZ0RpcmVjdGl2ZX0gZnJvbSAnLi9zdGlja3ktdGhpbmcuZGlyZWN0aXZlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW10sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIFN0aWNreVRoaW5nRGlyZWN0aXZlLFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgU3RpY2t5VGhpbmdEaXJlY3RpdmUsXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQW5ndWxhclN0aWNreVRoaW5nc01vZHVsZSB7XG59XG4iXX0=