/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Inject, Input, isDevMode, Output, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { BehaviorSubject, combineLatest, pipe, Subject } from 'rxjs';
import { animationFrame } from 'rxjs/internal/scheduler/animationFrame';
import { auditTime, filter, map, share, startWith, takeUntil, throttleTime } from 'rxjs/operators';
/**
 * @record
 */
export function StickyPositions() { }
if (false) {
    /** @type {?} */
    StickyPositions.prototype.offsetY;
    /** @type {?} */
    StickyPositions.prototype.bottomBoundary;
    /** @type {?|undefined} */
    StickyPositions.prototype.upperScreenEdgeAt;
    /** @type {?|undefined} */
    StickyPositions.prototype.marginTop;
    /** @type {?|undefined} */
    StickyPositions.prototype.marginBottom;
}
/**
 * @record
 */
export function StickyStatus() { }
if (false) {
    /** @type {?} */
    StickyStatus.prototype.isSticky;
    /** @type {?} */
    StickyStatus.prototype.reachedUpperEdge;
    /** @type {?} */
    StickyStatus.prototype.reachedLowerEdge;
    /** @type {?|undefined} */
    StickyStatus.prototype.marginTop;
    /** @type {?|undefined} */
    StickyStatus.prototype.marginBottom;
}
export class StickyThingDirective {
    /**
     * @param {?} stickyElement
     * @param {?} platformId
     */
    constructor(stickyElement, platformId) {
        this.stickyElement = stickyElement;
        this.platformId = platformId;
        this.filterGate = false;
        this.marginTop$ = new BehaviorSubject(0);
        this.marginBottom$ = new BehaviorSubject(0);
        this.enable$ = new BehaviorSubject(true);
        this.auditTime = 0;
        this.sticky = false;
        this.isSticky = false;
        this.boundaryReached = false;
        this.upperBoundReached = false;
        this.stickyStatus = new EventEmitter();
        this.stickyPosition = new EventEmitter();
        /**
         * The field represents some position values in normal (not sticky) mode.
         * If the browser size or the content of the page changes, this value must be recalculated.
         *
         */
        this.scroll$ = new Subject();
        this.target = this.getScrollTarget();
        this.resize$ = new Subject();
        this.extraordinaryChange$ = new BehaviorSubject(undefined);
        this.componentDestroyed = new Subject();
        this.listener = (/**
         * @param {?} e
         * @return {?}
         */
        (e) => {
            /** @type {?} */
            const upperScreenEdgeAt = ((/** @type {?} */ (e.target))).scrollTop || window.pageYOffset;
            this.scroll$.next(upperScreenEdgeAt);
        });
        /**
         * Throttle the scroll to animation frame (around 16.67ms) */
        this.scrollThrottled$ = this.scroll$
            .pipe(throttleTime(0, animationFrame), share());
        /**
         * Throttle the resize to animation frame (around 16.67ms) */
        this.resizeThrottled$ = this.resize$
            .pipe(throttleTime(0, animationFrame), 
        // emit once since we are currently using combineLatest
        startWith(null), share());
        this.status$ = combineLatest(this.enable$, this.scrollThrottled$, this.marginTop$, this.marginBottom$, this.extraordinaryChange$, this.resizeThrottled$)
            .pipe(filter((/**
         * @param {?} __0
         * @return {?}
         */
        ([enabled]) => this.checkEnabled(enabled))), map((/**
         * @param {?} __0
         * @return {?}
         */
        ([enabled, pageYOffset, marginTop, marginBottom]) => this.determineStatus(this.determineElementOffsets(), pageYOffset, marginTop, marginBottom, enabled))), share());
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set marginTop(value) {
        this.marginTop$.next(value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set marginBottom(value) {
        this.marginBottom$.next(value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set enable(value) {
        this.enable$.next(value);
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        /** @type {?} */
        const operators = this.scrollContainer ?
            pipe(takeUntil(this.componentDestroyed)) :
            pipe(auditTime(this.auditTime), takeUntil(this.componentDestroyed));
        this.status$
            .pipe(operators)
            .subscribe((/**
         * @param {?} status
         * @return {?}
         */
        (status) => {
            this.setSticky(status);
            this.setStatus(status);
        }));
    }
    /**
     * @return {?}
     */
    recalculate() {
        if (isPlatformBrowser(this.platformId)) {
            // Make sure to be in the next tick by using timeout
            setTimeout((/**
             * @return {?}
             */
            () => {
                this.extraordinaryChange$.next(undefined);
            }), 0);
        }
    }
    /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if \@Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     *
     * @param {?} enabled
     * @return {?}
     */
    checkEnabled(enabled) {
        if (!isPlatformBrowser(this.platformId)) {
            return false;
        }
        if (enabled) {
            // reset the gate
            this.filterGate = false;
            return true;
        }
        else {
            if (this.filterGate) {
                // gate closed, first emit has happened
                return false;
            }
            else {
                // this is the first emit for enabled = false,
                // let it pass, and activate the gate
                // so the next wont pass.
                this.filterGate = true;
                return true;
            }
        }
    }
    /**
     * @return {?}
     */
    onWindowResize() {
        if (isPlatformBrowser(this.platformId)) {
            this.resize$.next();
        }
    }
    /**
     * @return {?}
     */
    setupListener() {
        if (isPlatformBrowser(this.platformId)) {
            /** @type {?} */
            const target = this.getScrollTarget();
            target.addEventListener('scroll', this.listener);
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.checkSetup();
        this.setupListener();
        this.elementOffsetY = this.determineElementOffsets().offsetY;
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.target.removeEventListener('scroll', this.listener);
        this.componentDestroyed.next();
    }
    /**
     * @private
     * @return {?}
     */
    getScrollTarget() {
        /** @type {?} */
        let target;
        if (this.scrollContainer && typeof this.scrollContainer === 'string') {
            target = document.querySelector(this.scrollContainer);
            this.marginTop$.next(Infinity);
            this.auditTime = 0;
        }
        else if (this.scrollContainer && this.scrollContainer instanceof HTMLElement) {
            target = this.scrollContainer;
            this.marginTop$.next(Infinity);
            this.auditTime = 0;
        }
        else {
            target = window;
        }
        return target;
    }
    /**
     * @param {?} el
     * @return {?}
     */
    getComputedStyle(el) {
        return el.getBoundingClientRect();
    }
    /**
     * @private
     * @param {?} originalVals
     * @param {?} pageYOffset
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} enabled
     * @return {?}
     */
    determineStatus(originalVals, pageYOffset, marginTop, marginBottom, enabled) {
        /** @type {?} */
        const elementPos = this.determineElementOffsets();
        /** @type {?} */
        let isSticky = enabled && pageYOffset > originalVals.offsetY;
        if (pageYOffset < this.elementOffsetY) {
            isSticky = false;
        }
        /** @type {?} */
        const stickyElementHeight = this.getComputedStyle(this.stickyElement.nativeElement).height;
        // const reachedLowerEdge = isNotNullOrUndefined(this.boundaryElement) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
        // const reachedUpperEdge = isNotNullOrUndefined(this.boundaryElement) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
        /** @type {?} */
        const reachedLowerEdge = (this.boundaryElement != null) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
        /** @type {?} */
        const reachedUpperEdge = (this.boundaryElement != null) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
        this.stickyPosition.emit(Object.assign({}, elementPos, { upperScreenEdgeAt: pageYOffset, marginBottom, marginTop }));
        return {
            isSticky,
            reachedUpperEdge,
            reachedLowerEdge,
        };
    }
    // not always pixel. e.g. ie9
    /**
     * @private
     * @return {?}
     */
    getMargins() {
        /** @type {?} */
        const stickyStyles = window.getComputedStyle(this.stickyElement.nativeElement);
        /** @type {?} */
        const top = parseInt(stickyStyles.marginTop, 10);
        /** @type {?} */
        const bottom = parseInt(stickyStyles.marginBottom, 10);
        return { top, bottom };
    }
    /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements.
     * @private
     * @return {?}
     */
    determineElementOffsets() {
        if (this.sticky) {
            this.removeSticky();
        }
        /** @type {?} */
        let bottomBoundary = null;
        if (this.boundaryElement) {
            /** @type {?} */
            const boundaryElementHeight = this.getComputedStyle(this.boundaryElement).height;
            /** @type {?} */
            const boundaryElementOffset = getPosition(this.boundaryElement).y;
            bottomBoundary = boundaryElementHeight + boundaryElementOffset;
        }
        return {
            offsetY: (getPosition(this.stickyElement.nativeElement).y - this.marginTop$.value), bottomBoundary
        };
    }
    /**
     * @private
     * @param {?=} boundaryReached
     * @param {?=} marginTop
     * @param {?=} marginBottom
     * @return {?}
     */
    makeSticky(boundaryReached = false, marginTop, marginBottom) {
        // do this before setting it to pos:fixed
        const { width, height, left } = this.getComputedStyle(this.stickyElement.nativeElement);
        /** @type {?} */
        const offSet = boundaryReached ? (this.getComputedStyle(this.boundaryElement).bottom - height - this.marginBottom$.value) : this.marginTop$.value;
        if (this.scrollContainer && !this.sticky) {
            this.stickyElement.nativeElement.style.position = 'sticky';
            this.stickyElement.nativeElement.style.top = '0px';
            this.sticky = true;
        }
        else {
            this.stickyElement.nativeElement.style.position = 'fixed';
            this.stickyElement.nativeElement.style.top = offSet + 'px';
            this.stickyElement.nativeElement.style.left = left + 'px';
            this.stickyElement.nativeElement.style.width = `${width}px`;
        }
        if (this.spacerElement) {
            /** @type {?} */
            const spacerHeight = marginBottom + height + marginTop;
            this.spacerElement.style.height = `${spacerHeight}px`;
        }
    }
    /**
     * @private
     * @param {?} boundaryHeight
     * @param {?} stickyElHeight
     * @param {?} cssMargins
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} upperScreenEdgeAt
     * @return {?}
     */
    determineBoundaryReached(boundaryHeight, stickyElHeight, cssMargins, marginTop, marginBottom, upperScreenEdgeAt) {
        /** @type {?} */
        const boundaryElementPos = getPosition(this.boundaryElement);
        /** @type {?} */
        const boundaryElementLowerEdge = boundaryElementPos.y + boundaryHeight;
        /** @type {?} */
        const lowerEdgeStickyElement = upperScreenEdgeAt + stickyElHeight + marginTop + cssMargins.top + marginBottom + cssMargins.bottom;
        return boundaryElementLowerEdge <= lowerEdgeStickyElement;
    }
    /**
     * @private
     * @return {?}
     */
    checkSetup() {
        if (isDevMode() && !this.spacerElement) {
            console.warn(`******There might be an issue with your sticky directive!******

You haven't specified a spacer element. This will cause the page to jump.

Best practise is to provide a spacer element (e.g. a div) right before/after the sticky element.
Then pass the spacer element as input:

<div #spacer></div>

<div stickyThing="" [spacer]="spacer">
    I am sticky!
</div>`);
        }
    }
    /**
     * @private
     * @param {?} status
     * @return {?}
     */
    setSticky(status) {
        if (status.isSticky) {
            if (this.upperBoundReached) {
                this.removeSticky();
                this.isSticky = false;
            }
            else {
                this.makeSticky(status.reachedLowerEdge, status.marginTop, status.marginBottom);
                this.isSticky = true;
            }
        }
        else {
            this.removeSticky();
        }
    }
    /**
     * @private
     * @param {?} status
     * @return {?}
     */
    setStatus(status) {
        this.upperBoundReached = status.reachedUpperEdge;
        this.boundaryReached = status.reachedLowerEdge;
        this.stickyStatus.next(status);
    }
    /**
     * @private
     * @return {?}
     */
    removeSticky() {
        this.boundaryReached = false;
        this.sticky = false;
        this.stickyElement.nativeElement.style.position = '';
        this.stickyElement.nativeElement.style.width = 'auto';
        this.stickyElement.nativeElement.style.left = 'auto';
        this.stickyElement.nativeElement.style.top = 'auto';
        if (this.spacerElement) {
            this.spacerElement.style.height = '0';
        }
    }
}
StickyThingDirective.decorators = [
    { type: Directive, args: [{
                selector: '[stickyThing]'
            },] }
];
/** @nocollapse */
StickyThingDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: String, decorators: [{ type: Inject, args: [PLATFORM_ID,] }] }
];
StickyThingDirective.propDecorators = {
    scrollContainer: [{ type: Input }],
    auditTime: [{ type: Input }],
    marginTop: [{ type: Input }],
    marginBottom: [{ type: Input }],
    enable: [{ type: Input }],
    spacerElement: [{ type: Input, args: ['spacer',] }],
    boundaryElement: [{ type: Input, args: ['boundary',] }],
    isSticky: [{ type: HostBinding, args: ['class.is-sticky',] }],
    boundaryReached: [{ type: HostBinding, args: ['class.boundary-reached',] }],
    upperBoundReached: [{ type: HostBinding, args: ['class.upper-bound-reached',] }],
    stickyStatus: [{ type: Output }],
    stickyPosition: [{ type: Output }],
    onWindowResize: [{ type: HostListener, args: ['window:resize', [],] }]
};
if (false) {
    /** @type {?} */
    StickyThingDirective.prototype.filterGate;
    /** @type {?} */
    StickyThingDirective.prototype.marginTop$;
    /** @type {?} */
    StickyThingDirective.prototype.marginBottom$;
    /** @type {?} */
    StickyThingDirective.prototype.enable$;
    /** @type {?} */
    StickyThingDirective.prototype.scrollContainer;
    /** @type {?} */
    StickyThingDirective.prototype.auditTime;
    /** @type {?} */
    StickyThingDirective.prototype.spacerElement;
    /** @type {?} */
    StickyThingDirective.prototype.boundaryElement;
    /** @type {?} */
    StickyThingDirective.prototype.sticky;
    /** @type {?} */
    StickyThingDirective.prototype.isSticky;
    /** @type {?} */
    StickyThingDirective.prototype.boundaryReached;
    /** @type {?} */
    StickyThingDirective.prototype.upperBoundReached;
    /** @type {?} */
    StickyThingDirective.prototype.stickyStatus;
    /** @type {?} */
    StickyThingDirective.prototype.stickyPosition;
    /**
     * The field represents some position values in normal (not sticky) mode.
     * If the browser size or the content of the page changes, this value must be recalculated.
     *
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.scroll$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.scrollThrottled$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.target;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.resize$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.resizeThrottled$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.extraordinaryChange$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.status$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.componentDestroyed;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.elementOffsetY;
    /** @type {?} */
    StickyThingDirective.prototype.listener;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.stickyElement;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.platformId;
}
// Thanks to https://stanko.github.io/javascript-get-element-offset/
/**
 * @param {?} el
 * @return {?}
 */
function getPosition(el) {
    /** @type {?} */
    let top = 0;
    /** @type {?} */
    let left = 0;
    /** @type {?} */
    let element = el;
    // Loop through the DOM tree
    // and add it's parent's offset to get page offset
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);
    return {
        y: top,
        x: left,
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5LXRoaW5nLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B3MTFrL2FuZ3VsYXItc3RpY2t5LXRoaW5ncy8iLCJzb3VyY2VzIjpbImxpYi9zdGlja3ktdGhpbmcuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBRUwsU0FBUyxFQUNULFVBQVUsRUFDVixZQUFZLEVBQ1osV0FBVyxFQUNYLFlBQVksRUFDWixNQUFNLEVBQ04sS0FBSyxFQUNMLFNBQVMsRUFHVCxNQUFNLEVBQ04sV0FBVyxFQUNaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ2xELE9BQU8sRUFBQyxlQUFlLEVBQUUsYUFBYSxFQUFjLElBQUksRUFBRSxPQUFPLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDL0UsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RFLE9BQU8sRUFBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUlqRyxxQ0FNQzs7O0lBTEMsa0NBQWdCOztJQUNoQix5Q0FBOEI7O0lBQzlCLDRDQUEyQjs7SUFDM0Isb0NBQW1COztJQUNuQix1Q0FBc0I7Ozs7O0FBR3hCLGtDQU1DOzs7SUFMQyxnQ0FBa0I7O0lBQ2xCLHdDQUEwQjs7SUFDMUIsd0NBQTBCOztJQUMxQixpQ0FBbUI7O0lBQ25CLG9DQUFzQjs7QUFNeEIsTUFBTSxPQUFPLG9CQUFvQjs7Ozs7SUE4Qy9CLFlBQW9CLGFBQXlCLEVBQStCLFVBQWtCO1FBQTFFLGtCQUFhLEdBQWIsYUFBYSxDQUFZO1FBQStCLGVBQVUsR0FBVixVQUFVLENBQVE7UUE1QzlGLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLGtCQUFhLEdBQUcsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkMsWUFBTyxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRzNCLGNBQVMsR0FBRyxDQUFDLENBQUM7UUFjdkIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNpQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ1Ysb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDckIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFELGlCQUFZLEdBQStCLElBQUksWUFBWSxFQUFnQixDQUFDO1FBQzVFLG1CQUFjLEdBQWtDLElBQUksWUFBWSxFQUFtQixDQUFDOzs7Ozs7UUFNdEYsWUFBTyxHQUFHLElBQUksT0FBTyxFQUFVLENBQUM7UUFFaEMsV0FBTSxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUVoQyxZQUFPLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUU5Qix5QkFBb0IsR0FBRyxJQUFJLGVBQWUsQ0FBTyxTQUFTLENBQUMsQ0FBQztRQUk1RCx1QkFBa0IsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBOEdqRCxhQUFROzs7O1FBQUcsQ0FBQyxDQUFRLEVBQUUsRUFBRTs7a0JBQ2hCLGlCQUFpQixHQUFHLENBQUMsbUJBQUEsQ0FBQyxDQUFDLE1BQU0sRUFBZSxDQUFDLENBQUMsU0FBUyxJQUFJLE1BQU0sQ0FBQyxXQUFXO1lBQ25GLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDdkMsQ0FBQyxFQUFBO1FBNUdDO3FFQUM2RDtRQUM3RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE9BQU87YUFDakMsSUFBSSxDQUNILFlBQVksQ0FBQyxDQUFDLEVBQUUsY0FBYyxDQUFDLEVBQy9CLEtBQUssRUFBRSxDQUNSLENBQUM7UUFFSjtxRUFDNkQ7UUFDN0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPO2FBQ2pDLElBQUksQ0FDSCxZQUFZLENBQUMsQ0FBQyxFQUFFLGNBQWMsQ0FBQztRQUMvQix1REFBdUQ7UUFDdkQsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUNmLEtBQUssRUFBRSxDQUNSLENBQUM7UUFHSixJQUFJLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FDMUIsSUFBSSxDQUFDLE9BQU8sRUFDWixJQUFJLENBQUMsZ0JBQWdCLEVBQ3JCLElBQUksQ0FBQyxVQUFVLEVBQ2YsSUFBSSxDQUFDLGFBQWEsRUFDbEIsSUFBSSxDQUFDLG9CQUFvQixFQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQ3RCO2FBQ0UsSUFBSSxDQUNILE1BQU07Ozs7UUFBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEVBQUMsRUFDakQsR0FBRzs7OztRQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxZQUFZLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxPQUFPLENBQUMsRUFBQyxFQUM3SixLQUFLLEVBQUUsQ0FDUixDQUFDO0lBRU4sQ0FBQzs7Ozs7SUF4RUQsSUFBYSxTQUFTLENBQUMsS0FBYTtRQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7OztJQUVELElBQWEsWUFBWSxDQUFDLEtBQWE7UUFDckMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxJQUFhLE1BQU0sQ0FBQyxLQUFjO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7SUFnRUQsZUFBZTs7Y0FDUCxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsT0FBTzthQUNULElBQUksQ0FBQyxTQUFTLENBQUM7YUFDZixTQUFTOzs7O1FBQUMsQ0FBQyxNQUFvQixFQUFFLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVNLFdBQVc7UUFDaEIsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDdEMsb0RBQW9EO1lBQ3BELFVBQVU7OztZQUFDLEdBQUcsRUFBRTtnQkFDZCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVDLENBQUMsR0FBRSxDQUFDLENBQUMsQ0FBQztTQUNQO0lBQ0gsQ0FBQzs7Ozs7Ozs7Ozs7O0lBV0QsWUFBWSxDQUFDLE9BQWdCO1FBRTNCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDdkMsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUVELElBQUksT0FBTyxFQUFFO1lBQ1gsaUJBQWlCO1lBQ2pCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLE9BQU8sSUFBSSxDQUFDO1NBQ2I7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDbkIsdUNBQXVDO2dCQUN2QyxPQUFPLEtBQUssQ0FBQzthQUNkO2lCQUFNO2dCQUNMLDhDQUE4QztnQkFDOUMscUNBQXFDO2dCQUNyQyx5QkFBeUI7Z0JBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7SUFHSCxDQUFDOzs7O0lBR0QsY0FBYztRQUNaLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDckI7SUFDSCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNYLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFOztrQkFDaEMsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDckMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDbEQ7SUFDSCxDQUFDOzs7O0lBUUQsUUFBUTtRQUNOLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxPQUFPLENBQUM7SUFDL0QsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRU8sZUFBZTs7WUFDakIsTUFBd0I7UUFDNUIsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLE9BQU8sSUFBSSxDQUFDLGVBQWUsS0FBSyxRQUFRLEVBQUU7WUFDcEUsTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1NBQ3BCO2FBQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLFlBQVksV0FBVyxFQUFFO1lBQzlFLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1NBQ3BCO2FBQU07WUFDTCxNQUFNLEdBQUcsTUFBTSxDQUFDO1NBQ2pCO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFDRCxnQkFBZ0IsQ0FBQyxFQUFlO1FBQzlCLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixFQUFFLENBQUM7SUFDcEMsQ0FBQzs7Ozs7Ozs7OztJQUVPLGVBQWUsQ0FBQyxZQUE2QixFQUFFLFdBQW1CLEVBQUUsU0FBaUIsRUFBRSxZQUFvQixFQUFFLE9BQWdCOztjQUM3SCxVQUFVLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixFQUFFOztZQUM3QyxRQUFRLEdBQUcsT0FBTyxJQUFJLFdBQVcsR0FBRyxZQUFZLENBQUMsT0FBTztRQUM1RCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3JDLFFBQVEsR0FBRyxLQUFLLENBQUM7U0FDbEI7O2NBQ0ssbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsTUFBTTs7OztjQUdyRixnQkFBZ0IsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLElBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLElBQUksTUFBTSxDQUFDLFdBQVcsR0FBRyxtQkFBbUIsR0FBRyxZQUFZLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxHQUFHLFNBQVMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUzs7Y0FDL0wsZ0JBQWdCLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxJQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEdBQUcsU0FBUyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTO1FBQzNJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxtQkFBSyxVQUFVLElBQUUsaUJBQWlCLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxTQUFTLElBQUUsQ0FBQztRQUNuRyxPQUFPO1lBQ0wsUUFBUTtZQUNSLGdCQUFnQjtZQUNoQixnQkFBZ0I7U0FDakIsQ0FBQztJQUVKLENBQUM7Ozs7OztJQUlPLFVBQVU7O2NBQ1YsWUFBWSxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQzs7Y0FDeEUsR0FBRyxHQUFHLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQzs7Y0FDMUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQztRQUN0RCxPQUFPLEVBQUMsR0FBRyxFQUFFLE1BQU0sRUFBQyxDQUFDO0lBQ3ZCLENBQUM7Ozs7Ozs7OztJQU9PLHVCQUF1QjtRQUM3QixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDckI7O1lBQ0csY0FBYyxHQUFrQixJQUFJO1FBRXhDLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTs7a0JBQ2xCLHFCQUFxQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsTUFBTTs7a0JBQzFFLHFCQUFxQixHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUNqRSxjQUFjLEdBQUcscUJBQXFCLEdBQUcscUJBQXFCLENBQUM7U0FDaEU7UUFDRCxPQUFPO1lBQ0wsT0FBTyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUUsY0FBYztTQUNuRyxDQUFDO0lBQ0osQ0FBQzs7Ozs7Ozs7SUFFTyxVQUFVLENBQUMsa0JBQTJCLEtBQUssRUFBRSxTQUFpQixFQUFFLFlBQW9COztjQUVwRixFQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDOztjQUMvRSxNQUFNLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsTUFBTSxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUs7UUFDakosSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN4QyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUMzRCxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztZQUNuRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUNwQjthQUFNO1lBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7WUFDMUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzNELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQztZQUMxRCxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsS0FBSyxJQUFJLENBQUM7U0FDN0Q7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7O2tCQUNoQixZQUFZLEdBQUcsWUFBWSxHQUFHLE1BQU0sR0FBRyxTQUFTO1lBQ3RELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLFlBQVksSUFBSSxDQUFDO1NBQ3ZEO0lBQ0gsQ0FBQzs7Ozs7Ozs7Ozs7SUFFTyx3QkFBd0IsQ0FBQyxjQUFzQixFQUFFLGNBQXNCLEVBQUUsVUFBVSxFQUFFLFNBQWlCLEVBQUUsWUFBb0IsRUFBRSxpQkFBeUI7O2NBRXZKLGtCQUFrQixHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDOztjQUV0RCx3QkFBd0IsR0FBRyxrQkFBa0IsQ0FBQyxDQUFDLEdBQUcsY0FBYzs7Y0FDaEUsc0JBQXNCLEdBQUcsaUJBQWlCLEdBQUcsY0FBYyxHQUFHLFNBQVMsR0FBRyxVQUFVLENBQUMsR0FBRyxHQUFHLFlBQVksR0FBRyxVQUFVLENBQUMsTUFBTTtRQUVqSSxPQUFPLHdCQUF3QixJQUFJLHNCQUFzQixDQUFDO0lBQzVELENBQUM7Ozs7O0lBRU8sVUFBVTtRQUNoQixJQUFJLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QyxPQUFPLENBQUMsSUFBSSxDQUFDOzs7Ozs7Ozs7OztPQVdaLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7O0lBR08sU0FBUyxDQUFDLE1BQW9CO1FBQ3BDLElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUNuQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDaEYsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDdEI7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQzs7Ozs7O0lBRU8sU0FBUyxDQUFDLE1BQW9CO1FBQ3BDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUM7UUFDakQsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUM7UUFDL0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFTyxZQUFZO1FBQ2xCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDO1FBQ3RELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ3JELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDO1FBQ3BELElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1NBQ3ZDO0lBQ0gsQ0FBQzs7O1lBaFVGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTthQUMxQjs7OztZQXJDQyxVQUFVO3lDQW9Gc0MsTUFBTSxTQUFDLFdBQVc7Ozs4QkF2Q2pFLEtBQUs7d0JBQ0wsS0FBSzt3QkFDTCxLQUFLOzJCQUlMLEtBQUs7cUJBSUwsS0FBSzs0QkFHTCxLQUFLLFNBQUMsUUFBUTs4QkFDZCxLQUFLLFNBQUMsVUFBVTt1QkFFaEIsV0FBVyxTQUFDLGlCQUFpQjs4QkFDN0IsV0FBVyxTQUFDLHdCQUF3QjtnQ0FDcEMsV0FBVyxTQUFDLDJCQUEyQjsyQkFDdkMsTUFBTTs2QkFDTixNQUFNOzZCQWdITixZQUFZLFNBQUMsZUFBZSxFQUFFLEVBQUU7Ozs7SUF6SWpDLDBDQUFtQjs7SUFDbkIsMENBQW9DOztJQUNwQyw2Q0FBdUM7O0lBQ3ZDLHVDQUFvQzs7SUFFcEMsK0NBQTJEOztJQUMzRCx5Q0FBdUI7O0lBWXZCLDZDQUF3RDs7SUFDeEQsK0NBQTREOztJQUM1RCxzQ0FBZTs7SUFDZix3Q0FBaUQ7O0lBQ2pELCtDQUErRDs7SUFDL0QsaURBQW9FOztJQUNwRSw0Q0FBc0Y7O0lBQ3RGLDhDQUE4Rjs7Ozs7Ozs7SUFNOUYsdUNBQXdDOzs7OztJQUN4QyxnREFBNkM7Ozs7O0lBQzdDLHNDQUF3Qzs7Ozs7SUFFeEMsdUNBQXNDOzs7OztJQUN0QyxnREFBMkM7Ozs7O0lBQzNDLG9EQUFvRTs7Ozs7SUFFcEUsdUNBQTBDOzs7OztJQUUxQyxrREFBaUQ7Ozs7O0lBQ2pELDhDQUF1Qjs7SUE2R3ZCLHdDQUdDOzs7OztJQTlHVyw2Q0FBaUM7Ozs7O0lBQUUsMENBQStDOzs7Ozs7O0FBbVJoRyxTQUFTLFdBQVcsQ0FBQyxFQUFFOztRQUNqQixHQUFHLEdBQUcsQ0FBQzs7UUFDUCxJQUFJLEdBQUcsQ0FBQzs7UUFDUixPQUFPLEdBQUcsRUFBRTtJQUVoQiw0QkFBNEI7SUFDNUIsa0RBQWtEO0lBQ2xELEdBQUc7UUFDRCxHQUFHLElBQUksT0FBTyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUM7UUFDOUIsSUFBSSxJQUFJLE9BQU8sQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDO0tBQ2hDLFFBQVEsT0FBTyxFQUFFO0lBRWxCLE9BQU87UUFDTCxDQUFDLEVBQUUsR0FBRztRQUNOLENBQUMsRUFBRSxJQUFJO0tBQ1IsQ0FBQztBQUNKLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBBZnRlclZpZXdJbml0LFxuICBEaXJlY3RpdmUsXG4gIEVsZW1lbnRSZWYsXG4gIEV2ZW50RW1pdHRlcixcbiAgSG9zdEJpbmRpbmcsXG4gIEhvc3RMaXN0ZW5lcixcbiAgSW5qZWN0LFxuICBJbnB1dCxcbiAgaXNEZXZNb2RlLFxuICBPbkRlc3Ryb3ksXG4gIE9uSW5pdCxcbiAgT3V0cHV0LFxuICBQTEFURk9STV9JRFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7aXNQbGF0Zm9ybUJyb3dzZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0JlaGF2aW9yU3ViamVjdCwgY29tYmluZUxhdGVzdCwgT2JzZXJ2YWJsZSwgcGlwZSwgU3ViamVjdH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge2FuaW1hdGlvbkZyYW1lfSBmcm9tICdyeGpzL2ludGVybmFsL3NjaGVkdWxlci9hbmltYXRpb25GcmFtZSc7XG5pbXBvcnQge2F1ZGl0VGltZSwgZmlsdGVyLCBtYXAsIHNoYXJlLCBzdGFydFdpdGgsIHRha2VVbnRpbCwgdGhyb3R0bGVUaW1lfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG4vLyBpbXBvcnQge2lzTm90TnVsbE9yVW5kZWZpbmVkfSBmcm9tICdjb2RlbHl6ZXIvdXRpbC9pc05vdE51bGxPclVuZGVmaW5lZCc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBTdGlja3lQb3NpdGlvbnMge1xuICBvZmZzZXRZOiBudW1iZXI7XG4gIGJvdHRvbUJvdW5kYXJ5OiBudW1iZXIgfCBudWxsO1xuICB1cHBlclNjcmVlbkVkZ2VBdD86IG51bWJlcjtcbiAgbWFyZ2luVG9wPzogbnVtYmVyO1xuICBtYXJnaW5Cb3R0b20/OiBudW1iZXI7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgU3RpY2t5U3RhdHVzIHtcbiAgaXNTdGlja3k6IGJvb2xlYW47XG4gIHJlYWNoZWRVcHBlckVkZ2U6IGJvb2xlYW47XG4gIHJlYWNoZWRMb3dlckVkZ2U6IGJvb2xlYW47XG4gIG1hcmdpblRvcD86IG51bWJlcjtcbiAgbWFyZ2luQm90dG9tPzogbnVtYmVyO1xufVxuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbc3RpY2t5VGhpbmddJ1xufSlcbmV4cG9ydCBjbGFzcyBTdGlja3lUaGluZ0RpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IHtcblxuICBmaWx0ZXJHYXRlID0gZmFsc2U7XG4gIG1hcmdpblRvcCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KDApO1xuICBtYXJnaW5Cb3R0b20kID0gbmV3IEJlaGF2aW9yU3ViamVjdCgwKTtcbiAgZW5hYmxlJCA9IG5ldyBCZWhhdmlvclN1YmplY3QodHJ1ZSk7XG5cbiAgQElucHV0KCkgc2Nyb2xsQ29udGFpbmVyOiBzdHJpbmcgfCBIVE1MRWxlbWVudCB8IHVuZGVmaW5lZDtcbiAgQElucHV0KCkgYXVkaXRUaW1lID0gMDtcbiAgQElucHV0KCkgc2V0IG1hcmdpblRvcCh2YWx1ZTogbnVtYmVyKSB7XG4gICAgdGhpcy5tYXJnaW5Ub3AkLm5leHQodmFsdWUpO1xuICB9XG5cbiAgQElucHV0KCkgc2V0IG1hcmdpbkJvdHRvbSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgdGhpcy5tYXJnaW5Cb3R0b20kLm5leHQodmFsdWUpO1xuICB9XG5cbiAgQElucHV0KCkgc2V0IGVuYWJsZSh2YWx1ZTogYm9vbGVhbikge1xuICAgIHRoaXMuZW5hYmxlJC5uZXh0KHZhbHVlKTtcbiAgfVxuICBASW5wdXQoJ3NwYWNlcicpIHNwYWNlckVsZW1lbnQ6IEhUTUxFbGVtZW50IHwgdW5kZWZpbmVkO1xuICBASW5wdXQoJ2JvdW5kYXJ5JykgYm91bmRhcnlFbGVtZW50OiBIVE1MRWxlbWVudCB8IHVuZGVmaW5lZDtcbiAgc3RpY2t5ID0gZmFsc2U7XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaXMtc3RpY2t5JykgaXNTdGlja3kgPSBmYWxzZTtcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5ib3VuZGFyeS1yZWFjaGVkJykgYm91bmRhcnlSZWFjaGVkID0gZmFsc2U7XG4gIEBIb3N0QmluZGluZygnY2xhc3MudXBwZXItYm91bmQtcmVhY2hlZCcpIHVwcGVyQm91bmRSZWFjaGVkID0gZmFsc2U7XG4gIEBPdXRwdXQoKSBzdGlja3lTdGF0dXM6IEV2ZW50RW1pdHRlcjxTdGlja3lTdGF0dXM+ID0gbmV3IEV2ZW50RW1pdHRlcjxTdGlja3lTdGF0dXM+KCk7XG4gIEBPdXRwdXQoKSBzdGlja3lQb3NpdGlvbjogRXZlbnRFbWl0dGVyPFN0aWNreVBvc2l0aW9ucz4gPSBuZXcgRXZlbnRFbWl0dGVyPFN0aWNreVBvc2l0aW9ucz4oKTtcblxuICAvKipcbiAgICogVGhlIGZpZWxkIHJlcHJlc2VudHMgc29tZSBwb3NpdGlvbiB2YWx1ZXMgaW4gbm9ybWFsIChub3Qgc3RpY2t5KSBtb2RlLlxuICAgKiBJZiB0aGUgYnJvd3NlciBzaXplIG9yIHRoZSBjb250ZW50IG9mIHRoZSBwYWdlIGNoYW5nZXMsIHRoaXMgdmFsdWUgbXVzdCBiZSByZWNhbGN1bGF0ZWQuXG4gICAqICovXG4gIHByaXZhdGUgc2Nyb2xsJCA9IG5ldyBTdWJqZWN0PG51bWJlcj4oKTtcbiAgcHJpdmF0ZSBzY3JvbGxUaHJvdHRsZWQkOiBPYnNlcnZhYmxlPG51bWJlcj47XG4gIHByaXZhdGUgdGFyZ2V0ID0gdGhpcy5nZXRTY3JvbGxUYXJnZXQoKTtcblxuICBwcml2YXRlIHJlc2l6ZSQgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xuICBwcml2YXRlIHJlc2l6ZVRocm90dGxlZCQ6IE9ic2VydmFibGU8dm9pZD47XG4gIHByaXZhdGUgZXh0cmFvcmRpbmFyeUNoYW5nZSQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PHZvaWQ+KHVuZGVmaW5lZCk7XG5cbiAgcHJpdmF0ZSBzdGF0dXMkOiBPYnNlcnZhYmxlPFN0aWNreVN0YXR1cz47XG5cbiAgcHJpdmF0ZSBjb21wb25lbnREZXN0cm95ZWQgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xuICBwcml2YXRlIGVsZW1lbnRPZmZzZXRZO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc3RpY2t5RWxlbWVudDogRWxlbWVudFJlZiwgQEluamVjdChQTEFURk9STV9JRCkgcHJpdmF0ZSBwbGF0Zm9ybUlkOiBzdHJpbmcpIHtcblxuICAgIC8qKlxuICAgICAqIFRocm90dGxlIHRoZSBzY3JvbGwgdG8gYW5pbWF0aW9uIGZyYW1lIChhcm91bmQgMTYuNjdtcykgKi9cbiAgICB0aGlzLnNjcm9sbFRocm90dGxlZCQgPSB0aGlzLnNjcm9sbCRcbiAgICAgIC5waXBlKFxuICAgICAgICB0aHJvdHRsZVRpbWUoMCwgYW5pbWF0aW9uRnJhbWUpLFxuICAgICAgICBzaGFyZSgpXG4gICAgICApO1xuXG4gICAgLyoqXG4gICAgICogVGhyb3R0bGUgdGhlIHJlc2l6ZSB0byBhbmltYXRpb24gZnJhbWUgKGFyb3VuZCAxNi42N21zKSAqL1xuICAgIHRoaXMucmVzaXplVGhyb3R0bGVkJCA9IHRoaXMucmVzaXplJFxuICAgICAgLnBpcGUoXG4gICAgICAgIHRocm90dGxlVGltZSgwLCBhbmltYXRpb25GcmFtZSksXG4gICAgICAgIC8vIGVtaXQgb25jZSBzaW5jZSB3ZSBhcmUgY3VycmVudGx5IHVzaW5nIGNvbWJpbmVMYXRlc3RcbiAgICAgICAgc3RhcnRXaXRoKG51bGwpLFxuICAgICAgICBzaGFyZSgpXG4gICAgICApO1xuXG5cbiAgICB0aGlzLnN0YXR1cyQgPSBjb21iaW5lTGF0ZXN0KFxuICAgICAgdGhpcy5lbmFibGUkLFxuICAgICAgdGhpcy5zY3JvbGxUaHJvdHRsZWQkLFxuICAgICAgdGhpcy5tYXJnaW5Ub3AkLFxuICAgICAgdGhpcy5tYXJnaW5Cb3R0b20kLFxuICAgICAgdGhpcy5leHRyYW9yZGluYXJ5Q2hhbmdlJCxcbiAgICAgIHRoaXMucmVzaXplVGhyb3R0bGVkJCxcbiAgICApXG4gICAgICAucGlwZShcbiAgICAgICAgZmlsdGVyKChbZW5hYmxlZF0pID0+IHRoaXMuY2hlY2tFbmFibGVkKGVuYWJsZWQpKSxcbiAgICAgICAgbWFwKChbZW5hYmxlZCwgcGFnZVlPZmZzZXQsIG1hcmdpblRvcCwgbWFyZ2luQm90dG9tXSkgPT4gdGhpcy5kZXRlcm1pbmVTdGF0dXModGhpcy5kZXRlcm1pbmVFbGVtZW50T2Zmc2V0cygpLCBwYWdlWU9mZnNldCwgbWFyZ2luVG9wLCBtYXJnaW5Cb3R0b20sIGVuYWJsZWQpKSxcbiAgICAgICAgc2hhcmUoKSxcbiAgICAgICk7XG5cbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICBjb25zdCBvcGVyYXRvcnMgPSB0aGlzLnNjcm9sbENvbnRhaW5lciA/XG4gICAgICBwaXBlKHRha2VVbnRpbCh0aGlzLmNvbXBvbmVudERlc3Ryb3llZCkpIDpcbiAgICAgIHBpcGUoYXVkaXRUaW1lKHRoaXMuYXVkaXRUaW1lKSwgdGFrZVVudGlsKHRoaXMuY29tcG9uZW50RGVzdHJveWVkKSk7XG4gICAgdGhpcy5zdGF0dXMkXG4gICAgICAucGlwZShvcGVyYXRvcnMpXG4gICAgICAuc3Vic2NyaWJlKChzdGF0dXM6IFN0aWNreVN0YXR1cykgPT4ge1xuICAgICAgICB0aGlzLnNldFN0aWNreShzdGF0dXMpO1xuICAgICAgICB0aGlzLnNldFN0YXR1cyhzdGF0dXMpO1xuICAgICAgfSk7XG4gIH1cblxuICBwdWJsaWMgcmVjYWxjdWxhdGUoKTogdm9pZCB7XG4gICAgaWYgKGlzUGxhdGZvcm1Ccm93c2VyKHRoaXMucGxhdGZvcm1JZCkpIHtcbiAgICAgIC8vIE1ha2Ugc3VyZSB0byBiZSBpbiB0aGUgbmV4dCB0aWNrIGJ5IHVzaW5nIHRpbWVvdXRcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLmV4dHJhb3JkaW5hcnlDaGFuZ2UkLm5leHQodW5kZWZpbmVkKTtcbiAgICAgIH0sIDApO1xuICAgIH1cbiAgfVxuXG5cbiAgLyoqXG4gICAqIFRoaXMgaXMgbmFzdHkgY29kZSB0aGF0IHNob3VsZCBiZSByZWZhY3RvcmVkIGF0IHNvbWUgcG9pbnQuXG4gICAqXG4gICAqIFRoZSBQcm9ibGVtIGlzLCB3ZSBmaWx0ZXIgZm9yIGVuYWJsZWQuIFNvIHRoYXQgdGhlIGNvZGUgZG9lc24ndCBydW5cbiAgICogaWYgQElucHV0IGVuYWJsZWQgPSBmYWxzZS4gQnV0IGlmIHRoZSB1c2VyIGRpc2FibGVzLCB3ZSBuZWVkIGV4YWN0bHkgMVxuICAgKiBlbWl0IGluIG9yZGVyIHRvIHJlc2V0IGFuZCBjYWxsIHJlbW92ZVN0aWNreS4gU28gdGhpcyBtZXRob2QgYmFzaWNhbGx5XG4gICAqIHR1cm5zIHRoZSBmaWx0ZXIgaW4gXCJmaWx0ZXIsIGJ1dCBsZXQgdGhlIGZpcnN0IHBhc3NcIi5cbiAgICogKi9cbiAgY2hlY2tFbmFibGVkKGVuYWJsZWQ6IGJvb2xlYW4pOiBib29sZWFuIHtcblxuICAgIGlmICghaXNQbGF0Zm9ybUJyb3dzZXIodGhpcy5wbGF0Zm9ybUlkKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGlmIChlbmFibGVkKSB7XG4gICAgICAvLyByZXNldCB0aGUgZ2F0ZVxuICAgICAgdGhpcy5maWx0ZXJHYXRlID0gZmFsc2U7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHRoaXMuZmlsdGVyR2F0ZSkge1xuICAgICAgICAvLyBnYXRlIGNsb3NlZCwgZmlyc3QgZW1pdCBoYXMgaGFwcGVuZWRcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gdGhpcyBpcyB0aGUgZmlyc3QgZW1pdCBmb3IgZW5hYmxlZCA9IGZhbHNlLFxuICAgICAgICAvLyBsZXQgaXQgcGFzcywgYW5kIGFjdGl2YXRlIHRoZSBnYXRlXG4gICAgICAgIC8vIHNvIHRoZSBuZXh0IHdvbnQgcGFzcy5cbiAgICAgICAgdGhpcy5maWx0ZXJHYXRlID0gdHJ1ZTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfVxuXG5cbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnLCBbXSlcbiAgb25XaW5kb3dSZXNpemUoKTogdm9pZCB7XG4gICAgaWYgKGlzUGxhdGZvcm1Ccm93c2VyKHRoaXMucGxhdGZvcm1JZCkpIHtcbiAgICAgIHRoaXMucmVzaXplJC5uZXh0KCk7XG4gICAgfVxuICB9XG5cbiAgc2V0dXBMaXN0ZW5lcigpOiB2b2lkIHtcbiAgICBpZiAoaXNQbGF0Zm9ybUJyb3dzZXIodGhpcy5wbGF0Zm9ybUlkKSkge1xuICAgICAgY29uc3QgdGFyZ2V0ID0gdGhpcy5nZXRTY3JvbGxUYXJnZXQoKTtcbiAgICAgIHRhcmdldC5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmxpc3RlbmVyKTtcbiAgICB9XG4gIH1cblxuICBsaXN0ZW5lciA9IChlOiBFdmVudCkgPT4ge1xuICAgIGNvbnN0IHVwcGVyU2NyZWVuRWRnZUF0ID0gKGUudGFyZ2V0IGFzIEhUTUxFbGVtZW50KS5zY3JvbGxUb3AgfHwgd2luZG93LnBhZ2VZT2Zmc2V0O1xuICAgIHRoaXMuc2Nyb2xsJC5uZXh0KHVwcGVyU2NyZWVuRWRnZUF0KTtcbiAgfVxuXG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5jaGVja1NldHVwKCk7XG4gICAgdGhpcy5zZXR1cExpc3RlbmVyKCk7XG4gICAgdGhpcy5lbGVtZW50T2Zmc2V0WSA9IHRoaXMuZGV0ZXJtaW5lRWxlbWVudE9mZnNldHMoKS5vZmZzZXRZO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgdGhpcy50YXJnZXQucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5saXN0ZW5lcik7XG4gICAgdGhpcy5jb21wb25lbnREZXN0cm95ZWQubmV4dCgpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRTY3JvbGxUYXJnZXQoKTogRWxlbWVudCB8IFdpbmRvdyB7XG4gICAgbGV0IHRhcmdldDogRWxlbWVudCB8IFdpbmRvdztcbiAgICBpZiAodGhpcy5zY3JvbGxDb250YWluZXIgJiYgdHlwZW9mIHRoaXMuc2Nyb2xsQ29udGFpbmVyID09PSAnc3RyaW5nJykge1xuICAgICAgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLnNjcm9sbENvbnRhaW5lcik7XG4gICAgICB0aGlzLm1hcmdpblRvcCQubmV4dChJbmZpbml0eSk7XG4gICAgICB0aGlzLmF1ZGl0VGltZSA9IDA7XG4gICAgfSBlbHNlIGlmICh0aGlzLnNjcm9sbENvbnRhaW5lciAmJiB0aGlzLnNjcm9sbENvbnRhaW5lciBpbnN0YW5jZW9mIEhUTUxFbGVtZW50KSB7XG4gICAgICB0YXJnZXQgPSB0aGlzLnNjcm9sbENvbnRhaW5lcjtcbiAgICAgIHRoaXMubWFyZ2luVG9wJC5uZXh0KEluZmluaXR5KTtcbiAgICAgIHRoaXMuYXVkaXRUaW1lID0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGFyZ2V0ID0gd2luZG93O1xuICAgIH1cbiAgICByZXR1cm4gdGFyZ2V0O1xuICB9XG4gIGdldENvbXB1dGVkU3R5bGUoZWw6IEhUTUxFbGVtZW50KTogQ2xpZW50UmVjdCB8IERPTVJlY3Qge1xuICAgIHJldHVybiBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgfVxuXG4gIHByaXZhdGUgZGV0ZXJtaW5lU3RhdHVzKG9yaWdpbmFsVmFsczogU3RpY2t5UG9zaXRpb25zLCBwYWdlWU9mZnNldDogbnVtYmVyLCBtYXJnaW5Ub3A6IG51bWJlciwgbWFyZ2luQm90dG9tOiBudW1iZXIsIGVuYWJsZWQ6IGJvb2xlYW4pIHtcbiAgICBjb25zdCBlbGVtZW50UG9zID0gdGhpcy5kZXRlcm1pbmVFbGVtZW50T2Zmc2V0cygpO1xuICAgIGxldCBpc1N0aWNreSA9IGVuYWJsZWQgJiYgcGFnZVlPZmZzZXQgPiBvcmlnaW5hbFZhbHMub2Zmc2V0WTtcbiAgICBpZiAocGFnZVlPZmZzZXQgPCB0aGlzLmVsZW1lbnRPZmZzZXRZKSB7XG4gICAgICBpc1N0aWNreSA9IGZhbHNlO1xuICAgIH1cbiAgICBjb25zdCBzdGlja3lFbGVtZW50SGVpZ2h0ID0gdGhpcy5nZXRDb21wdXRlZFN0eWxlKHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50KS5oZWlnaHQ7XG4gICAgLy8gY29uc3QgcmVhY2hlZExvd2VyRWRnZSA9IGlzTm90TnVsbE9yVW5kZWZpbmVkKHRoaXMuYm91bmRhcnlFbGVtZW50KSA/IHRoaXMuYm91bmRhcnlFbGVtZW50ICYmIHdpbmRvdy5wYWdlWU9mZnNldCArIHN0aWNreUVsZW1lbnRIZWlnaHQgKyBtYXJnaW5Cb3R0b20gPj0gKG9yaWdpbmFsVmFscy5ib3R0b21Cb3VuZGFyeSAtIG1hcmdpblRvcCAqIDEuMCkgOiB1bmRlZmluZWQ7XG4gICAgLy8gY29uc3QgcmVhY2hlZFVwcGVyRWRnZSA9IGlzTm90TnVsbE9yVW5kZWZpbmVkKHRoaXMuYm91bmRhcnlFbGVtZW50KSA/IHdpbmRvdy5wYWdlWU9mZnNldCA8ICh0aGlzLmJvdW5kYXJ5RWxlbWVudC5vZmZzZXRUb3AgKyBtYXJnaW5Ub3AgKiAxLjApIDogdW5kZWZpbmVkO1xuXHQgIGNvbnN0IHJlYWNoZWRMb3dlckVkZ2UgPSAodGhpcy5ib3VuZGFyeUVsZW1lbnQhPW51bGwpID8gdGhpcy5ib3VuZGFyeUVsZW1lbnQgJiYgd2luZG93LnBhZ2VZT2Zmc2V0ICsgc3RpY2t5RWxlbWVudEhlaWdodCArIG1hcmdpbkJvdHRvbSA+PSAob3JpZ2luYWxWYWxzLmJvdHRvbUJvdW5kYXJ5IC0gbWFyZ2luVG9wICogMS4wKSA6IHVuZGVmaW5lZDtcbiAgICBjb25zdCByZWFjaGVkVXBwZXJFZGdlID0gKHRoaXMuYm91bmRhcnlFbGVtZW50IT1udWxsKSA/IHdpbmRvdy5wYWdlWU9mZnNldCA8ICh0aGlzLmJvdW5kYXJ5RWxlbWVudC5vZmZzZXRUb3AgKyBtYXJnaW5Ub3AgKiAxLjApIDogdW5kZWZpbmVkO1xuICAgIHRoaXMuc3RpY2t5UG9zaXRpb24uZW1pdCh7Li4uZWxlbWVudFBvcywgdXBwZXJTY3JlZW5FZGdlQXQ6IHBhZ2VZT2Zmc2V0LCBtYXJnaW5Cb3R0b20sIG1hcmdpblRvcH0pO1xuICAgIHJldHVybiB7XG4gICAgICBpc1N0aWNreSxcbiAgICAgIHJlYWNoZWRVcHBlckVkZ2UsXG4gICAgICByZWFjaGVkTG93ZXJFZGdlLFxuICAgIH07XG5cbiAgfVxuXG5cbiAgLy8gbm90IGFsd2F5cyBwaXhlbC4gZS5nLiBpZTlcbiAgcHJpdmF0ZSBnZXRNYXJnaW5zKCk6IHsgdG9wOiBudW1iZXIsIGJvdHRvbTogbnVtYmVyIH0ge1xuICAgIGNvbnN0IHN0aWNreVN0eWxlcyA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50KTtcbiAgICBjb25zdCB0b3AgPSBwYXJzZUludChzdGlja3lTdHlsZXMubWFyZ2luVG9wLCAxMCk7XG4gICAgY29uc3QgYm90dG9tID0gcGFyc2VJbnQoc3RpY2t5U3R5bGVzLm1hcmdpbkJvdHRvbSwgMTApO1xuICAgIHJldHVybiB7dG9wLCBib3R0b219O1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIG9mZnNldCBmb3IgZWxlbWVudC4gSWYgdGhlIGVsZW1lbnRcbiAgICogY3VycmVudGx5IGlzIHN0aWNreSwgaXQgd2lsbCBnZXQgcmVtb3ZlZFxuICAgKiB0byBhY2Nlc3MgdGhlIG9yaWdpbmFsIHBvc2l0aW9uLiBPdGhlclxuICAgKiB3aXNlIHRoaXMgd291bGQganVzdCBiZSAwIGZvciBmaXhlZCBlbGVtZW50cy4gKi9cbiAgcHJpdmF0ZSBkZXRlcm1pbmVFbGVtZW50T2Zmc2V0cygpOiBTdGlja3lQb3NpdGlvbnMge1xuICAgIGlmICh0aGlzLnN0aWNreSkge1xuICAgICAgdGhpcy5yZW1vdmVTdGlja3koKTtcbiAgICB9XG4gICAgbGV0IGJvdHRvbUJvdW5kYXJ5OiBudW1iZXIgfCBudWxsID0gbnVsbDtcblxuICAgIGlmICh0aGlzLmJvdW5kYXJ5RWxlbWVudCkge1xuICAgICAgY29uc3QgYm91bmRhcnlFbGVtZW50SGVpZ2h0ID0gdGhpcy5nZXRDb21wdXRlZFN0eWxlKHRoaXMuYm91bmRhcnlFbGVtZW50KS5oZWlnaHQ7XG4gICAgICBjb25zdCBib3VuZGFyeUVsZW1lbnRPZmZzZXQgPSBnZXRQb3NpdGlvbih0aGlzLmJvdW5kYXJ5RWxlbWVudCkueTtcbiAgICAgIGJvdHRvbUJvdW5kYXJ5ID0gYm91bmRhcnlFbGVtZW50SGVpZ2h0ICsgYm91bmRhcnlFbGVtZW50T2Zmc2V0O1xuICAgIH1cbiAgICByZXR1cm4ge1xuICAgICAgb2Zmc2V0WTogKGdldFBvc2l0aW9uKHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50KS55IC0gdGhpcy5tYXJnaW5Ub3AkLnZhbHVlKSwgYm90dG9tQm91bmRhcnlcbiAgICB9O1xuICB9XG5cbiAgcHJpdmF0ZSBtYWtlU3RpY2t5KGJvdW5kYXJ5UmVhY2hlZDogYm9vbGVhbiA9IGZhbHNlLCBtYXJnaW5Ub3A6IG51bWJlciwgbWFyZ2luQm90dG9tOiBudW1iZXIpOiB2b2lkIHtcbiAgICAvLyBkbyB0aGlzIGJlZm9yZSBzZXR0aW5nIGl0IHRvIHBvczpmaXhlZFxuICAgIGNvbnN0IHt3aWR0aCwgaGVpZ2h0LCBsZWZ0fSA9IHRoaXMuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudCk7XG4gICAgY29uc3Qgb2ZmU2V0ID0gYm91bmRhcnlSZWFjaGVkID8gKHRoaXMuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLmJvdW5kYXJ5RWxlbWVudCkuYm90dG9tIC0gaGVpZ2h0IC0gdGhpcy5tYXJnaW5Cb3R0b20kLnZhbHVlKSA6IHRoaXMubWFyZ2luVG9wJC52YWx1ZTtcbiAgICBpZiAodGhpcy5zY3JvbGxDb250YWluZXIgJiYgIXRoaXMuc3RpY2t5KSB7XG4gICAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS5wb3NpdGlvbiA9ICdzdGlja3knO1xuICAgICAgdGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc3R5bGUudG9wID0gJzBweCc7XG4gICAgICB0aGlzLnN0aWNreSA9IHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50LnN0eWxlLnBvc2l0aW9uID0gJ2ZpeGVkJztcbiAgICAgIHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50LnN0eWxlLnRvcCA9IG9mZlNldCArICdweCc7XG4gICAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS5sZWZ0ID0gbGVmdCArICdweCc7XG4gICAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS53aWR0aCA9IGAke3dpZHRofXB4YDtcbiAgICB9XG4gICAgaWYgKHRoaXMuc3BhY2VyRWxlbWVudCkge1xuICAgICAgY29uc3Qgc3BhY2VySGVpZ2h0ID0gbWFyZ2luQm90dG9tICsgaGVpZ2h0ICsgbWFyZ2luVG9wO1xuICAgICAgdGhpcy5zcGFjZXJFbGVtZW50LnN0eWxlLmhlaWdodCA9IGAke3NwYWNlckhlaWdodH1weGA7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBkZXRlcm1pbmVCb3VuZGFyeVJlYWNoZWQoYm91bmRhcnlIZWlnaHQ6IG51bWJlciwgc3RpY2t5RWxIZWlnaHQ6IG51bWJlciwgY3NzTWFyZ2lucywgbWFyZ2luVG9wOiBudW1iZXIsIG1hcmdpbkJvdHRvbTogbnVtYmVyLCB1cHBlclNjcmVlbkVkZ2VBdDogbnVtYmVyKSB7XG5cbiAgICBjb25zdCBib3VuZGFyeUVsZW1lbnRQb3MgPSBnZXRQb3NpdGlvbih0aGlzLmJvdW5kYXJ5RWxlbWVudCk7XG5cbiAgICBjb25zdCBib3VuZGFyeUVsZW1lbnRMb3dlckVkZ2UgPSBib3VuZGFyeUVsZW1lbnRQb3MueSArIGJvdW5kYXJ5SGVpZ2h0O1xuICAgIGNvbnN0IGxvd2VyRWRnZVN0aWNreUVsZW1lbnQgPSB1cHBlclNjcmVlbkVkZ2VBdCArIHN0aWNreUVsSGVpZ2h0ICsgbWFyZ2luVG9wICsgY3NzTWFyZ2lucy50b3AgKyBtYXJnaW5Cb3R0b20gKyBjc3NNYXJnaW5zLmJvdHRvbTtcblxuICAgIHJldHVybiBib3VuZGFyeUVsZW1lbnRMb3dlckVkZ2UgPD0gbG93ZXJFZGdlU3RpY2t5RWxlbWVudDtcbiAgfVxuXG4gIHByaXZhdGUgY2hlY2tTZXR1cCgpIHtcbiAgICBpZiAoaXNEZXZNb2RlKCkgJiYgIXRoaXMuc3BhY2VyRWxlbWVudCkge1xuICAgICAgY29uc29sZS53YXJuKGAqKioqKipUaGVyZSBtaWdodCBiZSBhbiBpc3N1ZSB3aXRoIHlvdXIgc3RpY2t5IGRpcmVjdGl2ZSEqKioqKipcblxuWW91IGhhdmVuJ3Qgc3BlY2lmaWVkIGEgc3BhY2VyIGVsZW1lbnQuIFRoaXMgd2lsbCBjYXVzZSB0aGUgcGFnZSB0byBqdW1wLlxuXG5CZXN0IHByYWN0aXNlIGlzIHRvIHByb3ZpZGUgYSBzcGFjZXIgZWxlbWVudCAoZS5nLiBhIGRpdikgcmlnaHQgYmVmb3JlL2FmdGVyIHRoZSBzdGlja3kgZWxlbWVudC5cblRoZW4gcGFzcyB0aGUgc3BhY2VyIGVsZW1lbnQgYXMgaW5wdXQ6XG5cbjxkaXYgI3NwYWNlcj48L2Rpdj5cblxuPGRpdiBzdGlja3lUaGluZz1cIlwiIFtzcGFjZXJdPVwic3BhY2VyXCI+XG4gICAgSSBhbSBzdGlja3khXG48L2Rpdj5gKTtcbiAgICB9XG4gIH1cblxuXG4gIHByaXZhdGUgc2V0U3RpY2t5KHN0YXR1czogU3RpY2t5U3RhdHVzKTogdm9pZCB7XG4gICAgaWYgKHN0YXR1cy5pc1N0aWNreSkge1xuICAgICAgaWYgKHRoaXMudXBwZXJCb3VuZFJlYWNoZWQpIHtcbiAgICAgICAgdGhpcy5yZW1vdmVTdGlja3koKTtcbiAgICAgICAgdGhpcy5pc1N0aWNreSA9IGZhbHNlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5tYWtlU3RpY2t5KHN0YXR1cy5yZWFjaGVkTG93ZXJFZGdlLCBzdGF0dXMubWFyZ2luVG9wLCBzdGF0dXMubWFyZ2luQm90dG9tKTtcbiAgICAgICAgdGhpcy5pc1N0aWNreSA9IHRydWU7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucmVtb3ZlU3RpY2t5KCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzZXRTdGF0dXMoc3RhdHVzOiBTdGlja3lTdGF0dXMpIHtcbiAgICB0aGlzLnVwcGVyQm91bmRSZWFjaGVkID0gc3RhdHVzLnJlYWNoZWRVcHBlckVkZ2U7XG4gICAgdGhpcy5ib3VuZGFyeVJlYWNoZWQgPSBzdGF0dXMucmVhY2hlZExvd2VyRWRnZTtcbiAgICB0aGlzLnN0aWNreVN0YXR1cy5uZXh0KHN0YXR1cyk7XG4gIH1cblxuICBwcml2YXRlIHJlbW92ZVN0aWNreSgpOiB2b2lkIHtcbiAgICB0aGlzLmJvdW5kYXJ5UmVhY2hlZCA9IGZhbHNlO1xuICAgIHRoaXMuc3RpY2t5ID0gZmFsc2U7XG4gICAgdGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc3R5bGUucG9zaXRpb24gPSAnJztcbiAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS53aWR0aCA9ICdhdXRvJztcbiAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS5sZWZ0ID0gJ2F1dG8nO1xuICAgIHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50LnN0eWxlLnRvcCA9ICdhdXRvJztcbiAgICBpZiAodGhpcy5zcGFjZXJFbGVtZW50KSB7XG4gICAgICB0aGlzLnNwYWNlckVsZW1lbnQuc3R5bGUuaGVpZ2h0ID0gJzAnO1xuICAgIH1cbiAgfVxufVxuXG4vLyBUaGFua3MgdG8gaHR0cHM6Ly9zdGFua28uZ2l0aHViLmlvL2phdmFzY3JpcHQtZ2V0LWVsZW1lbnQtb2Zmc2V0L1xuZnVuY3Rpb24gZ2V0UG9zaXRpb24oZWwpIHtcbiAgbGV0IHRvcCA9IDA7XG4gIGxldCBsZWZ0ID0gMDtcbiAgbGV0IGVsZW1lbnQgPSBlbDtcblxuICAvLyBMb29wIHRocm91Z2ggdGhlIERPTSB0cmVlXG4gIC8vIGFuZCBhZGQgaXQncyBwYXJlbnQncyBvZmZzZXQgdG8gZ2V0IHBhZ2Ugb2Zmc2V0XG4gIGRvIHtcbiAgICB0b3AgKz0gZWxlbWVudC5vZmZzZXRUb3AgfHwgMDtcbiAgICBsZWZ0ICs9IGVsZW1lbnQub2Zmc2V0TGVmdCB8fCAwO1xuICAgIGVsZW1lbnQgPSBlbGVtZW50Lm9mZnNldFBhcmVudDtcbiAgfSB3aGlsZSAoZWxlbWVudCk7XG5cbiAgcmV0dXJuIHtcbiAgICB5OiB0b3AsXG4gICAgeDogbGVmdCxcbiAgfTtcbn1cbiJdfQ==