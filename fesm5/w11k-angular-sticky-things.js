import { __read, __assign } from 'tslib';
import { EventEmitter, isDevMode, Directive, ElementRef, Inject, PLATFORM_ID, Input, HostBinding, Output, HostListener, NgModule } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { BehaviorSubject, Subject, combineLatest, pipe } from 'rxjs';
import { animationFrame } from 'rxjs/internal/scheduler/animationFrame';
import { throttleTime, share, startWith, filter, map, takeUntil, auditTime } from 'rxjs/operators';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function StickyPositions() { }
if (false) {
    /** @type {?} */
    StickyPositions.prototype.offsetY;
    /** @type {?} */
    StickyPositions.prototype.bottomBoundary;
    /** @type {?|undefined} */
    StickyPositions.prototype.upperScreenEdgeAt;
    /** @type {?|undefined} */
    StickyPositions.prototype.marginTop;
    /** @type {?|undefined} */
    StickyPositions.prototype.marginBottom;
}
/**
 * @record
 */
function StickyStatus() { }
if (false) {
    /** @type {?} */
    StickyStatus.prototype.isSticky;
    /** @type {?} */
    StickyStatus.prototype.reachedUpperEdge;
    /** @type {?} */
    StickyStatus.prototype.reachedLowerEdge;
    /** @type {?|undefined} */
    StickyStatus.prototype.marginTop;
    /** @type {?|undefined} */
    StickyStatus.prototype.marginBottom;
}
var StickyThingDirective = /** @class */ (function () {
    function StickyThingDirective(stickyElement, platformId) {
        var _this = this;
        this.stickyElement = stickyElement;
        this.platformId = platformId;
        this.filterGate = false;
        this.marginTop$ = new BehaviorSubject(0);
        this.marginBottom$ = new BehaviorSubject(0);
        this.enable$ = new BehaviorSubject(true);
        this.auditTime = 0;
        this.sticky = false;
        this.isSticky = false;
        this.boundaryReached = false;
        this.upperBoundReached = false;
        this.stickyStatus = new EventEmitter();
        this.stickyPosition = new EventEmitter();
        /**
         * The field represents some position values in normal (not sticky) mode.
         * If the browser size or the content of the page changes, this value must be recalculated.
         *
         */
        this.scroll$ = new Subject();
        this.target = this.getScrollTarget();
        this.resize$ = new Subject();
        this.extraordinaryChange$ = new BehaviorSubject(undefined);
        this.componentDestroyed = new Subject();
        this.listener = (/**
         * @param {?} e
         * @return {?}
         */
        function (e) {
            /** @type {?} */
            var upperScreenEdgeAt = ((/** @type {?} */ (e.target))).scrollTop || window.pageYOffset;
            _this.scroll$.next(upperScreenEdgeAt);
        });
        /**
         * Throttle the scroll to animation frame (around 16.67ms) */
        this.scrollThrottled$ = this.scroll$
            .pipe(throttleTime(0, animationFrame), share());
        /**
         * Throttle the resize to animation frame (around 16.67ms) */
        this.resizeThrottled$ = this.resize$
            .pipe(throttleTime(0, animationFrame), 
        // emit once since we are currently using combineLatest
        startWith(null), share());
        this.status$ = combineLatest(this.enable$, this.scrollThrottled$, this.marginTop$, this.marginBottom$, this.extraordinaryChange$, this.resizeThrottled$)
            .pipe(filter((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = __read(_a, 1), enabled = _b[0];
            return _this.checkEnabled(enabled);
        })), map((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = __read(_a, 4), enabled = _b[0], pageYOffset = _b[1], marginTop = _b[2], marginBottom = _b[3];
            return _this.determineStatus(_this.determineElementOffsets(), pageYOffset, marginTop, marginBottom, enabled);
        })), share());
    }
    Object.defineProperty(StickyThingDirective.prototype, "marginTop", {
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.marginTop$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StickyThingDirective.prototype, "marginBottom", {
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.marginBottom$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StickyThingDirective.prototype, "enable", {
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.enable$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var operators = this.scrollContainer ?
            pipe(takeUntil(this.componentDestroyed)) :
            pipe(auditTime(this.auditTime), takeUntil(this.componentDestroyed));
        this.status$
            .pipe(operators)
            .subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.setSticky(status);
            _this.setStatus(status);
        }));
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.recalculate = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (isPlatformBrowser(this.platformId)) {
            // Make sure to be in the next tick by using timeout
            setTimeout((/**
             * @return {?}
             */
            function () {
                _this.extraordinaryChange$.next(undefined);
            }), 0);
        }
    };
    /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if @Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     * */
    /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if \@Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     *
     * @param {?} enabled
     * @return {?}
     */
    StickyThingDirective.prototype.checkEnabled = /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if \@Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     *
     * @param {?} enabled
     * @return {?}
     */
    function (enabled) {
        if (!isPlatformBrowser(this.platformId)) {
            return false;
        }
        if (enabled) {
            // reset the gate
            this.filterGate = false;
            return true;
        }
        else {
            if (this.filterGate) {
                // gate closed, first emit has happened
                return false;
            }
            else {
                // this is the first emit for enabled = false,
                // let it pass, and activate the gate
                // so the next wont pass.
                this.filterGate = true;
                return true;
            }
        }
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.onWindowResize = /**
     * @return {?}
     */
    function () {
        if (isPlatformBrowser(this.platformId)) {
            this.resize$.next();
        }
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.setupListener = /**
     * @return {?}
     */
    function () {
        if (isPlatformBrowser(this.platformId)) {
            /** @type {?} */
            var target = this.getScrollTarget();
            target.addEventListener('scroll', this.listener);
        }
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.checkSetup();
        this.setupListener();
        this.elementOffsetY = this.determineElementOffsets().offsetY;
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.target.removeEventListener('scroll', this.listener);
        this.componentDestroyed.next();
    };
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.getScrollTarget = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var target;
        if (this.scrollContainer && typeof this.scrollContainer === 'string') {
            target = document.querySelector(this.scrollContainer);
            this.marginTop$.next(Infinity);
            this.auditTime = 0;
        }
        else if (this.scrollContainer && this.scrollContainer instanceof HTMLElement) {
            target = this.scrollContainer;
            this.marginTop$.next(Infinity);
            this.auditTime = 0;
        }
        else {
            target = window;
        }
        return target;
    };
    /**
     * @param {?} el
     * @return {?}
     */
    StickyThingDirective.prototype.getComputedStyle = /**
     * @param {?} el
     * @return {?}
     */
    function (el) {
        return el.getBoundingClientRect();
    };
    /**
     * @private
     * @param {?} originalVals
     * @param {?} pageYOffset
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} enabled
     * @return {?}
     */
    StickyThingDirective.prototype.determineStatus = /**
     * @private
     * @param {?} originalVals
     * @param {?} pageYOffset
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} enabled
     * @return {?}
     */
    function (originalVals, pageYOffset, marginTop, marginBottom, enabled) {
        /** @type {?} */
        var elementPos = this.determineElementOffsets();
        /** @type {?} */
        var isSticky = enabled && pageYOffset > originalVals.offsetY;
        if (pageYOffset < this.elementOffsetY) {
            isSticky = false;
        }
        /** @type {?} */
        var stickyElementHeight = this.getComputedStyle(this.stickyElement.nativeElement).height;
        // const reachedLowerEdge = isNotNullOrUndefined(this.boundaryElement) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
        // const reachedUpperEdge = isNotNullOrUndefined(this.boundaryElement) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
        /** @type {?} */
        var reachedLowerEdge = (this.boundaryElement != null) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
        /** @type {?} */
        var reachedUpperEdge = (this.boundaryElement != null) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
        this.stickyPosition.emit(__assign({}, elementPos, { upperScreenEdgeAt: pageYOffset, marginBottom: marginBottom, marginTop: marginTop }));
        return {
            isSticky: isSticky,
            reachedUpperEdge: reachedUpperEdge,
            reachedLowerEdge: reachedLowerEdge,
        };
    };
    // not always pixel. e.g. ie9
    // not always pixel. e.g. ie9
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.getMargins = 
    // not always pixel. e.g. ie9
    /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var stickyStyles = window.getComputedStyle(this.stickyElement.nativeElement);
        /** @type {?} */
        var top = parseInt(stickyStyles.marginTop, 10);
        /** @type {?} */
        var bottom = parseInt(stickyStyles.marginBottom, 10);
        return { top: top, bottom: bottom };
    };
    /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements. */
    /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements.
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.determineElementOffsets = /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements.
     * @private
     * @return {?}
     */
    function () {
        if (this.sticky) {
            this.removeSticky();
        }
        /** @type {?} */
        var bottomBoundary = null;
        if (this.boundaryElement) {
            /** @type {?} */
            var boundaryElementHeight = this.getComputedStyle(this.boundaryElement).height;
            /** @type {?} */
            var boundaryElementOffset = getPosition(this.boundaryElement).y;
            bottomBoundary = boundaryElementHeight + boundaryElementOffset;
        }
        return {
            offsetY: (getPosition(this.stickyElement.nativeElement).y - this.marginTop$.value), bottomBoundary: bottomBoundary
        };
    };
    /**
     * @private
     * @param {?=} boundaryReached
     * @param {?=} marginTop
     * @param {?=} marginBottom
     * @return {?}
     */
    StickyThingDirective.prototype.makeSticky = /**
     * @private
     * @param {?=} boundaryReached
     * @param {?=} marginTop
     * @param {?=} marginBottom
     * @return {?}
     */
    function (boundaryReached, marginTop, marginBottom) {
        if (boundaryReached === void 0) { boundaryReached = false; }
        // do this before setting it to pos:fixed
        var _a = this.getComputedStyle(this.stickyElement.nativeElement), width = _a.width, height = _a.height, left = _a.left;
        /** @type {?} */
        var offSet = boundaryReached ? (this.getComputedStyle(this.boundaryElement).bottom - height - this.marginBottom$.value) : this.marginTop$.value;
        if (this.scrollContainer && !this.sticky) {
            this.stickyElement.nativeElement.style.position = 'sticky';
            this.stickyElement.nativeElement.style.top = '0px';
            this.sticky = true;
        }
        else {
            this.stickyElement.nativeElement.style.position = 'fixed';
            this.stickyElement.nativeElement.style.top = offSet + 'px';
            this.stickyElement.nativeElement.style.left = left + 'px';
            this.stickyElement.nativeElement.style.width = width + "px";
        }
        if (this.spacerElement) {
            /** @type {?} */
            var spacerHeight = marginBottom + height + marginTop;
            this.spacerElement.style.height = spacerHeight + "px";
        }
    };
    /**
     * @private
     * @param {?} boundaryHeight
     * @param {?} stickyElHeight
     * @param {?} cssMargins
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} upperScreenEdgeAt
     * @return {?}
     */
    StickyThingDirective.prototype.determineBoundaryReached = /**
     * @private
     * @param {?} boundaryHeight
     * @param {?} stickyElHeight
     * @param {?} cssMargins
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} upperScreenEdgeAt
     * @return {?}
     */
    function (boundaryHeight, stickyElHeight, cssMargins, marginTop, marginBottom, upperScreenEdgeAt) {
        /** @type {?} */
        var boundaryElementPos = getPosition(this.boundaryElement);
        /** @type {?} */
        var boundaryElementLowerEdge = boundaryElementPos.y + boundaryHeight;
        /** @type {?} */
        var lowerEdgeStickyElement = upperScreenEdgeAt + stickyElHeight + marginTop + cssMargins.top + marginBottom + cssMargins.bottom;
        return boundaryElementLowerEdge <= lowerEdgeStickyElement;
    };
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.checkSetup = /**
     * @private
     * @return {?}
     */
    function () {
        if (isDevMode() && !this.spacerElement) {
            console.warn("******There might be an issue with your sticky directive!******\n\nYou haven't specified a spacer element. This will cause the page to jump.\n\nBest practise is to provide a spacer element (e.g. a div) right before/after the sticky element.\nThen pass the spacer element as input:\n\n<div #spacer></div>\n\n<div stickyThing=\"\" [spacer]=\"spacer\">\n    I am sticky!\n</div>");
        }
    };
    /**
     * @private
     * @param {?} status
     * @return {?}
     */
    StickyThingDirective.prototype.setSticky = /**
     * @private
     * @param {?} status
     * @return {?}
     */
    function (status) {
        if (status.isSticky) {
            if (this.upperBoundReached) {
                this.removeSticky();
                this.isSticky = false;
            }
            else {
                this.makeSticky(status.reachedLowerEdge, status.marginTop, status.marginBottom);
                this.isSticky = true;
            }
        }
        else {
            this.removeSticky();
        }
    };
    /**
     * @private
     * @param {?} status
     * @return {?}
     */
    StickyThingDirective.prototype.setStatus = /**
     * @private
     * @param {?} status
     * @return {?}
     */
    function (status) {
        this.upperBoundReached = status.reachedUpperEdge;
        this.boundaryReached = status.reachedLowerEdge;
        this.stickyStatus.next(status);
    };
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.removeSticky = /**
     * @private
     * @return {?}
     */
    function () {
        this.boundaryReached = false;
        this.sticky = false;
        this.stickyElement.nativeElement.style.position = '';
        this.stickyElement.nativeElement.style.width = 'auto';
        this.stickyElement.nativeElement.style.left = 'auto';
        this.stickyElement.nativeElement.style.top = 'auto';
        if (this.spacerElement) {
            this.spacerElement.style.height = '0';
        }
    };
    StickyThingDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[stickyThing]'
                },] }
    ];
    /** @nocollapse */
    StickyThingDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: String, decorators: [{ type: Inject, args: [PLATFORM_ID,] }] }
    ]; };
    StickyThingDirective.propDecorators = {
        scrollContainer: [{ type: Input }],
        auditTime: [{ type: Input }],
        marginTop: [{ type: Input }],
        marginBottom: [{ type: Input }],
        enable: [{ type: Input }],
        spacerElement: [{ type: Input, args: ['spacer',] }],
        boundaryElement: [{ type: Input, args: ['boundary',] }],
        isSticky: [{ type: HostBinding, args: ['class.is-sticky',] }],
        boundaryReached: [{ type: HostBinding, args: ['class.boundary-reached',] }],
        upperBoundReached: [{ type: HostBinding, args: ['class.upper-bound-reached',] }],
        stickyStatus: [{ type: Output }],
        stickyPosition: [{ type: Output }],
        onWindowResize: [{ type: HostListener, args: ['window:resize', [],] }]
    };
    return StickyThingDirective;
}());
if (false) {
    /** @type {?} */
    StickyThingDirective.prototype.filterGate;
    /** @type {?} */
    StickyThingDirective.prototype.marginTop$;
    /** @type {?} */
    StickyThingDirective.prototype.marginBottom$;
    /** @type {?} */
    StickyThingDirective.prototype.enable$;
    /** @type {?} */
    StickyThingDirective.prototype.scrollContainer;
    /** @type {?} */
    StickyThingDirective.prototype.auditTime;
    /** @type {?} */
    StickyThingDirective.prototype.spacerElement;
    /** @type {?} */
    StickyThingDirective.prototype.boundaryElement;
    /** @type {?} */
    StickyThingDirective.prototype.sticky;
    /** @type {?} */
    StickyThingDirective.prototype.isSticky;
    /** @type {?} */
    StickyThingDirective.prototype.boundaryReached;
    /** @type {?} */
    StickyThingDirective.prototype.upperBoundReached;
    /** @type {?} */
    StickyThingDirective.prototype.stickyStatus;
    /** @type {?} */
    StickyThingDirective.prototype.stickyPosition;
    /**
     * The field represents some position values in normal (not sticky) mode.
     * If the browser size or the content of the page changes, this value must be recalculated.
     *
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.scroll$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.scrollThrottled$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.target;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.resize$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.resizeThrottled$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.extraordinaryChange$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.status$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.componentDestroyed;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.elementOffsetY;
    /** @type {?} */
    StickyThingDirective.prototype.listener;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.stickyElement;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.platformId;
}
// Thanks to https://stanko.github.io/javascript-get-element-offset/
/**
 * @param {?} el
 * @return {?}
 */
function getPosition(el) {
    /** @type {?} */
    var top = 0;
    /** @type {?} */
    var left = 0;
    /** @type {?} */
    var element = el;
    // Loop through the DOM tree
    // and add it's parent's offset to get page offset
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);
    return {
        y: top,
        x: left,
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AngularStickyThingsModule = /** @class */ (function () {
    function AngularStickyThingsModule() {
    }
    AngularStickyThingsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [
                        StickyThingDirective,
                    ],
                    exports: [
                        StickyThingDirective,
                    ]
                },] }
    ];
    return AngularStickyThingsModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { AngularStickyThingsModule, StickyThingDirective };
//# sourceMappingURL=w11k-angular-sticky-things.js.map
