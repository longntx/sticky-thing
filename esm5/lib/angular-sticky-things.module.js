/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { StickyThingDirective } from './sticky-thing.directive';
var AngularStickyThingsModule = /** @class */ (function () {
    function AngularStickyThingsModule() {
    }
    AngularStickyThingsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [
                        StickyThingDirective,
                    ],
                    exports: [
                        StickyThingDirective,
                    ]
                },] }
    ];
    return AngularStickyThingsModule;
}());
export { AngularStickyThingsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1zdGlja3ktdGhpbmdzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B3MTFrL2FuZ3VsYXItc3RpY2t5LXRoaW5ncy8iLCJzb3VyY2VzIjpbImxpYi9hbmd1bGFyLXN0aWNreS10aGluZ3MubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLDBCQUEwQixDQUFDO0FBRTlEO0lBQUE7SUFVQSxDQUFDOztnQkFWQSxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsWUFBWSxFQUFFO3dCQUNaLG9CQUFvQjtxQkFDckI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLG9CQUFvQjtxQkFDckI7aUJBQ0Y7O0lBRUQsZ0NBQUM7Q0FBQSxBQVZELElBVUM7U0FEWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7U3RpY2t5VGhpbmdEaXJlY3RpdmV9IGZyb20gJy4vc3RpY2t5LXRoaW5nLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBTdGlja3lUaGluZ0RpcmVjdGl2ZSxcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIFN0aWNreVRoaW5nRGlyZWN0aXZlLFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEFuZ3VsYXJTdGlja3lUaGluZ3NNb2R1bGUge1xufVxuIl19