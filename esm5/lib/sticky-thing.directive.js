/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Inject, Input, isDevMode, Output, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { BehaviorSubject, combineLatest, pipe, Subject } from 'rxjs';
import { animationFrame } from 'rxjs/internal/scheduler/animationFrame';
import { auditTime, filter, map, share, startWith, takeUntil, throttleTime } from 'rxjs/operators';
/**
 * @record
 */
export function StickyPositions() { }
if (false) {
    /** @type {?} */
    StickyPositions.prototype.offsetY;
    /** @type {?} */
    StickyPositions.prototype.bottomBoundary;
    /** @type {?|undefined} */
    StickyPositions.prototype.upperScreenEdgeAt;
    /** @type {?|undefined} */
    StickyPositions.prototype.marginTop;
    /** @type {?|undefined} */
    StickyPositions.prototype.marginBottom;
}
/**
 * @record
 */
export function StickyStatus() { }
if (false) {
    /** @type {?} */
    StickyStatus.prototype.isSticky;
    /** @type {?} */
    StickyStatus.prototype.reachedUpperEdge;
    /** @type {?} */
    StickyStatus.prototype.reachedLowerEdge;
    /** @type {?|undefined} */
    StickyStatus.prototype.marginTop;
    /** @type {?|undefined} */
    StickyStatus.prototype.marginBottom;
}
var StickyThingDirective = /** @class */ (function () {
    function StickyThingDirective(stickyElement, platformId) {
        var _this = this;
        this.stickyElement = stickyElement;
        this.platformId = platformId;
        this.filterGate = false;
        this.marginTop$ = new BehaviorSubject(0);
        this.marginBottom$ = new BehaviorSubject(0);
        this.enable$ = new BehaviorSubject(true);
        this.auditTime = 0;
        this.sticky = false;
        this.isSticky = false;
        this.boundaryReached = false;
        this.upperBoundReached = false;
        this.stickyStatus = new EventEmitter();
        this.stickyPosition = new EventEmitter();
        /**
         * The field represents some position values in normal (not sticky) mode.
         * If the browser size or the content of the page changes, this value must be recalculated.
         *
         */
        this.scroll$ = new Subject();
        this.target = this.getScrollTarget();
        this.resize$ = new Subject();
        this.extraordinaryChange$ = new BehaviorSubject(undefined);
        this.componentDestroyed = new Subject();
        this.listener = (/**
         * @param {?} e
         * @return {?}
         */
        function (e) {
            /** @type {?} */
            var upperScreenEdgeAt = ((/** @type {?} */ (e.target))).scrollTop || window.pageYOffset;
            _this.scroll$.next(upperScreenEdgeAt);
        });
        /**
         * Throttle the scroll to animation frame (around 16.67ms) */
        this.scrollThrottled$ = this.scroll$
            .pipe(throttleTime(0, animationFrame), share());
        /**
         * Throttle the resize to animation frame (around 16.67ms) */
        this.resizeThrottled$ = this.resize$
            .pipe(throttleTime(0, animationFrame), 
        // emit once since we are currently using combineLatest
        startWith(null), share());
        this.status$ = combineLatest(this.enable$, this.scrollThrottled$, this.marginTop$, this.marginBottom$, this.extraordinaryChange$, this.resizeThrottled$)
            .pipe(filter((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = tslib_1.__read(_a, 1), enabled = _b[0];
            return _this.checkEnabled(enabled);
        })), map((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = tslib_1.__read(_a, 4), enabled = _b[0], pageYOffset = _b[1], marginTop = _b[2], marginBottom = _b[3];
            return _this.determineStatus(_this.determineElementOffsets(), pageYOffset, marginTop, marginBottom, enabled);
        })), share());
    }
    Object.defineProperty(StickyThingDirective.prototype, "marginTop", {
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.marginTop$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StickyThingDirective.prototype, "marginBottom", {
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.marginBottom$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StickyThingDirective.prototype, "enable", {
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.enable$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var operators = this.scrollContainer ?
            pipe(takeUntil(this.componentDestroyed)) :
            pipe(auditTime(this.auditTime), takeUntil(this.componentDestroyed));
        this.status$
            .pipe(operators)
            .subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.setSticky(status);
            _this.setStatus(status);
        }));
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.recalculate = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (isPlatformBrowser(this.platformId)) {
            // Make sure to be in the next tick by using timeout
            setTimeout((/**
             * @return {?}
             */
            function () {
                _this.extraordinaryChange$.next(undefined);
            }), 0);
        }
    };
    /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if @Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     * */
    /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if \@Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     *
     * @param {?} enabled
     * @return {?}
     */
    StickyThingDirective.prototype.checkEnabled = /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if \@Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     *
     * @param {?} enabled
     * @return {?}
     */
    function (enabled) {
        if (!isPlatformBrowser(this.platformId)) {
            return false;
        }
        if (enabled) {
            // reset the gate
            this.filterGate = false;
            return true;
        }
        else {
            if (this.filterGate) {
                // gate closed, first emit has happened
                return false;
            }
            else {
                // this is the first emit for enabled = false,
                // let it pass, and activate the gate
                // so the next wont pass.
                this.filterGate = true;
                return true;
            }
        }
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.onWindowResize = /**
     * @return {?}
     */
    function () {
        if (isPlatformBrowser(this.platformId)) {
            this.resize$.next();
        }
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.setupListener = /**
     * @return {?}
     */
    function () {
        if (isPlatformBrowser(this.platformId)) {
            /** @type {?} */
            var target = this.getScrollTarget();
            target.addEventListener('scroll', this.listener);
        }
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.checkSetup();
        this.setupListener();
        this.elementOffsetY = this.determineElementOffsets().offsetY;
    };
    /**
     * @return {?}
     */
    StickyThingDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.target.removeEventListener('scroll', this.listener);
        this.componentDestroyed.next();
    };
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.getScrollTarget = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var target;
        if (this.scrollContainer && typeof this.scrollContainer === 'string') {
            target = document.querySelector(this.scrollContainer);
            this.marginTop$.next(Infinity);
            this.auditTime = 0;
        }
        else if (this.scrollContainer && this.scrollContainer instanceof HTMLElement) {
            target = this.scrollContainer;
            this.marginTop$.next(Infinity);
            this.auditTime = 0;
        }
        else {
            target = window;
        }
        return target;
    };
    /**
     * @param {?} el
     * @return {?}
     */
    StickyThingDirective.prototype.getComputedStyle = /**
     * @param {?} el
     * @return {?}
     */
    function (el) {
        return el.getBoundingClientRect();
    };
    /**
     * @private
     * @param {?} originalVals
     * @param {?} pageYOffset
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} enabled
     * @return {?}
     */
    StickyThingDirective.prototype.determineStatus = /**
     * @private
     * @param {?} originalVals
     * @param {?} pageYOffset
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} enabled
     * @return {?}
     */
    function (originalVals, pageYOffset, marginTop, marginBottom, enabled) {
        /** @type {?} */
        var elementPos = this.determineElementOffsets();
        /** @type {?} */
        var isSticky = enabled && pageYOffset > originalVals.offsetY;
        if (pageYOffset < this.elementOffsetY) {
            isSticky = false;
        }
        /** @type {?} */
        var stickyElementHeight = this.getComputedStyle(this.stickyElement.nativeElement).height;
        // const reachedLowerEdge = isNotNullOrUndefined(this.boundaryElement) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
        // const reachedUpperEdge = isNotNullOrUndefined(this.boundaryElement) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
        /** @type {?} */
        var reachedLowerEdge = (this.boundaryElement != null) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
        /** @type {?} */
        var reachedUpperEdge = (this.boundaryElement != null) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
        this.stickyPosition.emit(tslib_1.__assign({}, elementPos, { upperScreenEdgeAt: pageYOffset, marginBottom: marginBottom, marginTop: marginTop }));
        return {
            isSticky: isSticky,
            reachedUpperEdge: reachedUpperEdge,
            reachedLowerEdge: reachedLowerEdge,
        };
    };
    // not always pixel. e.g. ie9
    // not always pixel. e.g. ie9
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.getMargins = 
    // not always pixel. e.g. ie9
    /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var stickyStyles = window.getComputedStyle(this.stickyElement.nativeElement);
        /** @type {?} */
        var top = parseInt(stickyStyles.marginTop, 10);
        /** @type {?} */
        var bottom = parseInt(stickyStyles.marginBottom, 10);
        return { top: top, bottom: bottom };
    };
    /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements. */
    /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements.
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.determineElementOffsets = /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements.
     * @private
     * @return {?}
     */
    function () {
        if (this.sticky) {
            this.removeSticky();
        }
        /** @type {?} */
        var bottomBoundary = null;
        if (this.boundaryElement) {
            /** @type {?} */
            var boundaryElementHeight = this.getComputedStyle(this.boundaryElement).height;
            /** @type {?} */
            var boundaryElementOffset = getPosition(this.boundaryElement).y;
            bottomBoundary = boundaryElementHeight + boundaryElementOffset;
        }
        return {
            offsetY: (getPosition(this.stickyElement.nativeElement).y - this.marginTop$.value), bottomBoundary: bottomBoundary
        };
    };
    /**
     * @private
     * @param {?=} boundaryReached
     * @param {?=} marginTop
     * @param {?=} marginBottom
     * @return {?}
     */
    StickyThingDirective.prototype.makeSticky = /**
     * @private
     * @param {?=} boundaryReached
     * @param {?=} marginTop
     * @param {?=} marginBottom
     * @return {?}
     */
    function (boundaryReached, marginTop, marginBottom) {
        if (boundaryReached === void 0) { boundaryReached = false; }
        // do this before setting it to pos:fixed
        var _a = this.getComputedStyle(this.stickyElement.nativeElement), width = _a.width, height = _a.height, left = _a.left;
        /** @type {?} */
        var offSet = boundaryReached ? (this.getComputedStyle(this.boundaryElement).bottom - height - this.marginBottom$.value) : this.marginTop$.value;
        if (this.scrollContainer && !this.sticky) {
            this.stickyElement.nativeElement.style.position = 'sticky';
            this.stickyElement.nativeElement.style.top = '0px';
            this.sticky = true;
        }
        else {
            this.stickyElement.nativeElement.style.position = 'fixed';
            this.stickyElement.nativeElement.style.top = offSet + 'px';
            this.stickyElement.nativeElement.style.left = left + 'px';
            this.stickyElement.nativeElement.style.width = width + "px";
        }
        if (this.spacerElement) {
            /** @type {?} */
            var spacerHeight = marginBottom + height + marginTop;
            this.spacerElement.style.height = spacerHeight + "px";
        }
    };
    /**
     * @private
     * @param {?} boundaryHeight
     * @param {?} stickyElHeight
     * @param {?} cssMargins
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} upperScreenEdgeAt
     * @return {?}
     */
    StickyThingDirective.prototype.determineBoundaryReached = /**
     * @private
     * @param {?} boundaryHeight
     * @param {?} stickyElHeight
     * @param {?} cssMargins
     * @param {?} marginTop
     * @param {?} marginBottom
     * @param {?} upperScreenEdgeAt
     * @return {?}
     */
    function (boundaryHeight, stickyElHeight, cssMargins, marginTop, marginBottom, upperScreenEdgeAt) {
        /** @type {?} */
        var boundaryElementPos = getPosition(this.boundaryElement);
        /** @type {?} */
        var boundaryElementLowerEdge = boundaryElementPos.y + boundaryHeight;
        /** @type {?} */
        var lowerEdgeStickyElement = upperScreenEdgeAt + stickyElHeight + marginTop + cssMargins.top + marginBottom + cssMargins.bottom;
        return boundaryElementLowerEdge <= lowerEdgeStickyElement;
    };
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.checkSetup = /**
     * @private
     * @return {?}
     */
    function () {
        if (isDevMode() && !this.spacerElement) {
            console.warn("******There might be an issue with your sticky directive!******\n\nYou haven't specified a spacer element. This will cause the page to jump.\n\nBest practise is to provide a spacer element (e.g. a div) right before/after the sticky element.\nThen pass the spacer element as input:\n\n<div #spacer></div>\n\n<div stickyThing=\"\" [spacer]=\"spacer\">\n    I am sticky!\n</div>");
        }
    };
    /**
     * @private
     * @param {?} status
     * @return {?}
     */
    StickyThingDirective.prototype.setSticky = /**
     * @private
     * @param {?} status
     * @return {?}
     */
    function (status) {
        if (status.isSticky) {
            if (this.upperBoundReached) {
                this.removeSticky();
                this.isSticky = false;
            }
            else {
                this.makeSticky(status.reachedLowerEdge, status.marginTop, status.marginBottom);
                this.isSticky = true;
            }
        }
        else {
            this.removeSticky();
        }
    };
    /**
     * @private
     * @param {?} status
     * @return {?}
     */
    StickyThingDirective.prototype.setStatus = /**
     * @private
     * @param {?} status
     * @return {?}
     */
    function (status) {
        this.upperBoundReached = status.reachedUpperEdge;
        this.boundaryReached = status.reachedLowerEdge;
        this.stickyStatus.next(status);
    };
    /**
     * @private
     * @return {?}
     */
    StickyThingDirective.prototype.removeSticky = /**
     * @private
     * @return {?}
     */
    function () {
        this.boundaryReached = false;
        this.sticky = false;
        this.stickyElement.nativeElement.style.position = '';
        this.stickyElement.nativeElement.style.width = 'auto';
        this.stickyElement.nativeElement.style.left = 'auto';
        this.stickyElement.nativeElement.style.top = 'auto';
        if (this.spacerElement) {
            this.spacerElement.style.height = '0';
        }
    };
    StickyThingDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[stickyThing]'
                },] }
    ];
    /** @nocollapse */
    StickyThingDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: String, decorators: [{ type: Inject, args: [PLATFORM_ID,] }] }
    ]; };
    StickyThingDirective.propDecorators = {
        scrollContainer: [{ type: Input }],
        auditTime: [{ type: Input }],
        marginTop: [{ type: Input }],
        marginBottom: [{ type: Input }],
        enable: [{ type: Input }],
        spacerElement: [{ type: Input, args: ['spacer',] }],
        boundaryElement: [{ type: Input, args: ['boundary',] }],
        isSticky: [{ type: HostBinding, args: ['class.is-sticky',] }],
        boundaryReached: [{ type: HostBinding, args: ['class.boundary-reached',] }],
        upperBoundReached: [{ type: HostBinding, args: ['class.upper-bound-reached',] }],
        stickyStatus: [{ type: Output }],
        stickyPosition: [{ type: Output }],
        onWindowResize: [{ type: HostListener, args: ['window:resize', [],] }]
    };
    return StickyThingDirective;
}());
export { StickyThingDirective };
if (false) {
    /** @type {?} */
    StickyThingDirective.prototype.filterGate;
    /** @type {?} */
    StickyThingDirective.prototype.marginTop$;
    /** @type {?} */
    StickyThingDirective.prototype.marginBottom$;
    /** @type {?} */
    StickyThingDirective.prototype.enable$;
    /** @type {?} */
    StickyThingDirective.prototype.scrollContainer;
    /** @type {?} */
    StickyThingDirective.prototype.auditTime;
    /** @type {?} */
    StickyThingDirective.prototype.spacerElement;
    /** @type {?} */
    StickyThingDirective.prototype.boundaryElement;
    /** @type {?} */
    StickyThingDirective.prototype.sticky;
    /** @type {?} */
    StickyThingDirective.prototype.isSticky;
    /** @type {?} */
    StickyThingDirective.prototype.boundaryReached;
    /** @type {?} */
    StickyThingDirective.prototype.upperBoundReached;
    /** @type {?} */
    StickyThingDirective.prototype.stickyStatus;
    /** @type {?} */
    StickyThingDirective.prototype.stickyPosition;
    /**
     * The field represents some position values in normal (not sticky) mode.
     * If the browser size or the content of the page changes, this value must be recalculated.
     *
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.scroll$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.scrollThrottled$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.target;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.resize$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.resizeThrottled$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.extraordinaryChange$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.status$;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.componentDestroyed;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.elementOffsetY;
    /** @type {?} */
    StickyThingDirective.prototype.listener;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.stickyElement;
    /**
     * @type {?}
     * @private
     */
    StickyThingDirective.prototype.platformId;
}
// Thanks to https://stanko.github.io/javascript-get-element-offset/
/**
 * @param {?} el
 * @return {?}
 */
function getPosition(el) {
    /** @type {?} */
    var top = 0;
    /** @type {?} */
    var left = 0;
    /** @type {?} */
    var element = el;
    // Loop through the DOM tree
    // and add it's parent's offset to get page offset
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);
    return {
        y: top,
        x: left,
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5LXRoaW5nLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B3MTFrL2FuZ3VsYXItc3RpY2t5LXRoaW5ncy8iLCJzb3VyY2VzIjpbImxpYi9zdGlja3ktdGhpbmcuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUVMLFNBQVMsRUFDVCxVQUFVLEVBQ1YsWUFBWSxFQUNaLFdBQVcsRUFDWCxZQUFZLEVBQ1osTUFBTSxFQUNOLEtBQUssRUFDTCxTQUFTLEVBR1QsTUFBTSxFQUNOLFdBQVcsRUFDWixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUNsRCxPQUFPLEVBQUMsZUFBZSxFQUFFLGFBQWEsRUFBYyxJQUFJLEVBQUUsT0FBTyxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBQy9FLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSx3Q0FBd0MsQ0FBQztBQUN0RSxPQUFPLEVBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFJakcscUNBTUM7OztJQUxDLGtDQUFnQjs7SUFDaEIseUNBQThCOztJQUM5Qiw0Q0FBMkI7O0lBQzNCLG9DQUFtQjs7SUFDbkIsdUNBQXNCOzs7OztBQUd4QixrQ0FNQzs7O0lBTEMsZ0NBQWtCOztJQUNsQix3Q0FBMEI7O0lBQzFCLHdDQUEwQjs7SUFDMUIsaUNBQW1COztJQUNuQixvQ0FBc0I7O0FBR3hCO0lBaURFLDhCQUFvQixhQUF5QixFQUErQixVQUFrQjtRQUE5RixpQkFtQ0M7UUFuQ21CLGtCQUFhLEdBQWIsYUFBYSxDQUFZO1FBQStCLGVBQVUsR0FBVixVQUFVLENBQVE7UUE1QzlGLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLGtCQUFhLEdBQUcsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkMsWUFBTyxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRzNCLGNBQVMsR0FBRyxDQUFDLENBQUM7UUFjdkIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNpQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ1Ysb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDckIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFELGlCQUFZLEdBQStCLElBQUksWUFBWSxFQUFnQixDQUFDO1FBQzVFLG1CQUFjLEdBQWtDLElBQUksWUFBWSxFQUFtQixDQUFDOzs7Ozs7UUFNdEYsWUFBTyxHQUFHLElBQUksT0FBTyxFQUFVLENBQUM7UUFFaEMsV0FBTSxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUVoQyxZQUFPLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUU5Qix5QkFBb0IsR0FBRyxJQUFJLGVBQWUsQ0FBTyxTQUFTLENBQUMsQ0FBQztRQUk1RCx1QkFBa0IsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBOEdqRCxhQUFROzs7O1FBQUcsVUFBQyxDQUFROztnQkFDWixpQkFBaUIsR0FBRyxDQUFDLG1CQUFBLENBQUMsQ0FBQyxNQUFNLEVBQWUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsV0FBVztZQUNuRixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsRUFBQTtRQTVHQztxRUFDNkQ7UUFDN0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPO2FBQ2pDLElBQUksQ0FDSCxZQUFZLENBQUMsQ0FBQyxFQUFFLGNBQWMsQ0FBQyxFQUMvQixLQUFLLEVBQUUsQ0FDUixDQUFDO1FBRUo7cUVBQzZEO1FBQzdELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTzthQUNqQyxJQUFJLENBQ0gsWUFBWSxDQUFDLENBQUMsRUFBRSxjQUFjLENBQUM7UUFDL0IsdURBQXVEO1FBQ3ZELFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFDZixLQUFLLEVBQUUsQ0FDUixDQUFDO1FBR0osSUFBSSxDQUFDLE9BQU8sR0FBRyxhQUFhLENBQzFCLElBQUksQ0FBQyxPQUFPLEVBQ1osSUFBSSxDQUFDLGdCQUFnQixFQUNyQixJQUFJLENBQUMsVUFBVSxFQUNmLElBQUksQ0FBQyxhQUFhLEVBQ2xCLElBQUksQ0FBQyxvQkFBb0IsRUFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUN0QjthQUNFLElBQUksQ0FDSCxNQUFNOzs7O1FBQUMsVUFBQyxFQUFTO2dCQUFULDBCQUFTLEVBQVIsZUFBTztZQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUM7UUFBMUIsQ0FBMEIsRUFBQyxFQUNqRCxHQUFHOzs7O1FBQUMsVUFBQyxFQUErQztnQkFBL0MsMEJBQStDLEVBQTlDLGVBQU8sRUFBRSxtQkFBVyxFQUFFLGlCQUFTLEVBQUUsb0JBQVk7WUFBTSxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLHVCQUF1QixFQUFFLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsT0FBTyxDQUFDO1FBQW5HLENBQW1HLEVBQUMsRUFDN0osS0FBSyxFQUFFLENBQ1IsQ0FBQztJQUVOLENBQUM7SUF4RUQsc0JBQWEsMkNBQVM7Ozs7O1FBQXRCLFVBQXVCLEtBQWE7WUFDbEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBYSw4Q0FBWTs7Ozs7UUFBekIsVUFBMEIsS0FBYTtZQUNyQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFhLHdDQUFNOzs7OztRQUFuQixVQUFvQixLQUFjO1lBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLENBQUM7OztPQUFBOzs7O0lBZ0VELDhDQUFlOzs7SUFBZjtRQUFBLGlCQVVDOztZQVRPLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxPQUFPO2FBQ1QsSUFBSSxDQUFDLFNBQVMsQ0FBQzthQUNmLFNBQVM7Ozs7UUFBQyxVQUFDLE1BQW9CO1lBQzlCLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdkIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFTSwwQ0FBVzs7O0lBQWxCO1FBQUEsaUJBT0M7UUFOQyxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUN0QyxvREFBb0Q7WUFDcEQsVUFBVTs7O1lBQUM7Z0JBQ1QsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QyxDQUFDLEdBQUUsQ0FBQyxDQUFDLENBQUM7U0FDUDtJQUNILENBQUM7SUFHRDs7Ozs7OztTQU9LOzs7Ozs7Ozs7Ozs7SUFDTCwyQ0FBWTs7Ozs7Ozs7Ozs7SUFBWixVQUFhLE9BQWdCO1FBRTNCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDdkMsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUVELElBQUksT0FBTyxFQUFFO1lBQ1gsaUJBQWlCO1lBQ2pCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLE9BQU8sSUFBSSxDQUFDO1NBQ2I7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDbkIsdUNBQXVDO2dCQUN2QyxPQUFPLEtBQUssQ0FBQzthQUNkO2lCQUFNO2dCQUNMLDhDQUE4QztnQkFDOUMscUNBQXFDO2dCQUNyQyx5QkFBeUI7Z0JBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7SUFHSCxDQUFDOzs7O0lBR0QsNkNBQWM7OztJQURkO1FBRUUsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNyQjtJQUNILENBQUM7Ozs7SUFFRCw0Q0FBYTs7O0lBQWI7UUFDRSxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTs7Z0JBQ2hDLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3JDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2xEO0lBQ0gsQ0FBQzs7OztJQVFELHVDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxPQUFPLENBQUM7SUFDL0QsQ0FBQzs7OztJQUVELDBDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFTyw4Q0FBZTs7OztJQUF2Qjs7WUFDTSxNQUF3QjtRQUM1QixJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksT0FBTyxJQUFJLENBQUMsZUFBZSxLQUFLLFFBQVEsRUFBRTtZQUNwRSxNQUFNLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7U0FDcEI7YUFBTSxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsWUFBWSxXQUFXLEVBQUU7WUFDOUUsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7WUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7U0FDcEI7YUFBTTtZQUNMLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDakI7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOzs7OztJQUNELCtDQUFnQjs7OztJQUFoQixVQUFpQixFQUFlO1FBQzlCLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixFQUFFLENBQUM7SUFDcEMsQ0FBQzs7Ozs7Ozs7OztJQUVPLDhDQUFlOzs7Ozs7Ozs7SUFBdkIsVUFBd0IsWUFBNkIsRUFBRSxXQUFtQixFQUFFLFNBQWlCLEVBQUUsWUFBb0IsRUFBRSxPQUFnQjs7WUFDN0gsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsRUFBRTs7WUFDN0MsUUFBUSxHQUFHLE9BQU8sSUFBSSxXQUFXLEdBQUcsWUFBWSxDQUFDLE9BQU87UUFDNUQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1NBQ2xCOztZQUNLLG1CQUFtQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLE1BQU07Ozs7WUFHckYsZ0JBQWdCLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxJQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxJQUFJLE1BQU0sQ0FBQyxXQUFXLEdBQUcsbUJBQW1CLEdBQUcsWUFBWSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsR0FBRyxTQUFTLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVM7O1lBQy9MLGdCQUFnQixHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsSUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLFNBQVMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUztRQUMzSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksc0JBQUssVUFBVSxJQUFFLGlCQUFpQixFQUFFLFdBQVcsRUFBRSxZQUFZLGNBQUEsRUFBRSxTQUFTLFdBQUEsSUFBRSxDQUFDO1FBQ25HLE9BQU87WUFDTCxRQUFRLFVBQUE7WUFDUixnQkFBZ0Isa0JBQUE7WUFDaEIsZ0JBQWdCLGtCQUFBO1NBQ2pCLENBQUM7SUFFSixDQUFDO0lBR0QsNkJBQTZCOzs7Ozs7SUFDckIseUNBQVU7Ozs7OztJQUFsQjs7WUFDUSxZQUFZLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDOztZQUN4RSxHQUFHLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDOztZQUMxQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDO1FBQ3RELE9BQU8sRUFBQyxHQUFHLEtBQUEsRUFBRSxNQUFNLFFBQUEsRUFBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7Ozt1REFJbUQ7Ozs7Ozs7OztJQUMzQyxzREFBdUI7Ozs7Ozs7O0lBQS9CO1FBQ0UsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3JCOztZQUNHLGNBQWMsR0FBa0IsSUFBSTtRQUV4QyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7O2dCQUNsQixxQkFBcUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLE1BQU07O2dCQUMxRSxxQkFBcUIsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFDakUsY0FBYyxHQUFHLHFCQUFxQixHQUFHLHFCQUFxQixDQUFDO1NBQ2hFO1FBQ0QsT0FBTztZQUNMLE9BQU8sRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLGNBQWMsZ0JBQUE7U0FDbkcsQ0FBQztJQUNKLENBQUM7Ozs7Ozs7O0lBRU8seUNBQVU7Ozs7Ozs7SUFBbEIsVUFBbUIsZUFBZ0MsRUFBRSxTQUFpQixFQUFFLFlBQW9CO1FBQXpFLGdDQUFBLEVBQUEsdUJBQWdDOztRQUUzQyxJQUFBLDREQUErRSxFQUE5RSxnQkFBSyxFQUFFLGtCQUFNLEVBQUUsY0FBK0Q7O1lBQy9FLE1BQU0sR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSztRQUNqSixJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1lBQzNELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1lBQ25ELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3BCO2FBQU07WUFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztZQUMxRCxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDM0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQzFELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQU0sS0FBSyxPQUFJLENBQUM7U0FDN0Q7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7O2dCQUNoQixZQUFZLEdBQUcsWUFBWSxHQUFHLE1BQU0sR0FBRyxTQUFTO1lBQ3RELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBTSxZQUFZLE9BQUksQ0FBQztTQUN2RDtJQUNILENBQUM7Ozs7Ozs7Ozs7O0lBRU8sdURBQXdCOzs7Ozs7Ozs7O0lBQWhDLFVBQWlDLGNBQXNCLEVBQUUsY0FBc0IsRUFBRSxVQUFVLEVBQUUsU0FBaUIsRUFBRSxZQUFvQixFQUFFLGlCQUF5Qjs7WUFFdkosa0JBQWtCLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7O1lBRXRELHdCQUF3QixHQUFHLGtCQUFrQixDQUFDLENBQUMsR0FBRyxjQUFjOztZQUNoRSxzQkFBc0IsR0FBRyxpQkFBaUIsR0FBRyxjQUFjLEdBQUcsU0FBUyxHQUFHLFVBQVUsQ0FBQyxHQUFHLEdBQUcsWUFBWSxHQUFHLFVBQVUsQ0FBQyxNQUFNO1FBRWpJLE9BQU8sd0JBQXdCLElBQUksc0JBQXNCLENBQUM7SUFDNUQsQ0FBQzs7Ozs7SUFFTyx5Q0FBVTs7OztJQUFsQjtRQUNFLElBQUksU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RDLE9BQU8sQ0FBQyxJQUFJLENBQUMseVhBV1osQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7Ozs7SUFHTyx3Q0FBUzs7Ozs7SUFBakIsVUFBa0IsTUFBb0I7UUFDcEMsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO1lBQ25CLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO2dCQUMxQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNoRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN0QjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDckI7SUFDSCxDQUFDOzs7Ozs7SUFFTyx3Q0FBUzs7Ozs7SUFBakIsVUFBa0IsTUFBb0I7UUFDcEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztRQUNqRCxJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztRQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVPLDJDQUFZOzs7O0lBQXBCO1FBQ0UsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDckQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUM7UUFDdEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7UUFDckQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUM7UUFDcEQsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7U0FDdkM7SUFDSCxDQUFDOztnQkFoVUYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxlQUFlO2lCQUMxQjs7OztnQkFyQ0MsVUFBVTs2Q0FvRnNDLE1BQU0sU0FBQyxXQUFXOzs7a0NBdkNqRSxLQUFLOzRCQUNMLEtBQUs7NEJBQ0wsS0FBSzsrQkFJTCxLQUFLO3lCQUlMLEtBQUs7Z0NBR0wsS0FBSyxTQUFDLFFBQVE7a0NBQ2QsS0FBSyxTQUFDLFVBQVU7MkJBRWhCLFdBQVcsU0FBQyxpQkFBaUI7a0NBQzdCLFdBQVcsU0FBQyx3QkFBd0I7b0NBQ3BDLFdBQVcsU0FBQywyQkFBMkI7K0JBQ3ZDLE1BQU07aUNBQ04sTUFBTTtpQ0FnSE4sWUFBWSxTQUFDLGVBQWUsRUFBRSxFQUFFOztJQW1MbkMsMkJBQUM7Q0FBQSxBQWpVRCxJQWlVQztTQTlUWSxvQkFBb0I7OztJQUUvQiwwQ0FBbUI7O0lBQ25CLDBDQUFvQzs7SUFDcEMsNkNBQXVDOztJQUN2Qyx1Q0FBb0M7O0lBRXBDLCtDQUEyRDs7SUFDM0QseUNBQXVCOztJQVl2Qiw2Q0FBd0Q7O0lBQ3hELCtDQUE0RDs7SUFDNUQsc0NBQWU7O0lBQ2Ysd0NBQWlEOztJQUNqRCwrQ0FBK0Q7O0lBQy9ELGlEQUFvRTs7SUFDcEUsNENBQXNGOztJQUN0Riw4Q0FBOEY7Ozs7Ozs7O0lBTTlGLHVDQUF3Qzs7Ozs7SUFDeEMsZ0RBQTZDOzs7OztJQUM3QyxzQ0FBd0M7Ozs7O0lBRXhDLHVDQUFzQzs7Ozs7SUFDdEMsZ0RBQTJDOzs7OztJQUMzQyxvREFBb0U7Ozs7O0lBRXBFLHVDQUEwQzs7Ozs7SUFFMUMsa0RBQWlEOzs7OztJQUNqRCw4Q0FBdUI7O0lBNkd2Qix3Q0FHQzs7Ozs7SUE5R1csNkNBQWlDOzs7OztJQUFFLDBDQUErQzs7Ozs7OztBQW1SaEcsU0FBUyxXQUFXLENBQUMsRUFBRTs7UUFDakIsR0FBRyxHQUFHLENBQUM7O1FBQ1AsSUFBSSxHQUFHLENBQUM7O1FBQ1IsT0FBTyxHQUFHLEVBQUU7SUFFaEIsNEJBQTRCO0lBQzVCLGtEQUFrRDtJQUNsRCxHQUFHO1FBQ0QsR0FBRyxJQUFJLE9BQU8sQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksSUFBSSxPQUFPLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQztRQUNoQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztLQUNoQyxRQUFRLE9BQU8sRUFBRTtJQUVsQixPQUFPO1FBQ0wsQ0FBQyxFQUFFLEdBQUc7UUFDTixDQUFDLEVBQUUsSUFBSTtLQUNSLENBQUM7QUFDSixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQWZ0ZXJWaWV3SW5pdCxcbiAgRGlyZWN0aXZlLFxuICBFbGVtZW50UmVmLFxuICBFdmVudEVtaXR0ZXIsXG4gIEhvc3RCaW5kaW5nLFxuICBIb3N0TGlzdGVuZXIsXG4gIEluamVjdCxcbiAgSW5wdXQsXG4gIGlzRGV2TW9kZSxcbiAgT25EZXN0cm95LFxuICBPbkluaXQsXG4gIE91dHB1dCxcbiAgUExBVEZPUk1fSURcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge2lzUGxhdGZvcm1Ccm93c2VyfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtCZWhhdmlvclN1YmplY3QsIGNvbWJpbmVMYXRlc3QsIE9ic2VydmFibGUsIHBpcGUsIFN1YmplY3R9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHthbmltYXRpb25GcmFtZX0gZnJvbSAncnhqcy9pbnRlcm5hbC9zY2hlZHVsZXIvYW5pbWF0aW9uRnJhbWUnO1xuaW1wb3J0IHthdWRpdFRpbWUsIGZpbHRlciwgbWFwLCBzaGFyZSwgc3RhcnRXaXRoLCB0YWtlVW50aWwsIHRocm90dGxlVGltZX0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuLy8gaW1wb3J0IHtpc05vdE51bGxPclVuZGVmaW5lZH0gZnJvbSAnY29kZWx5emVyL3V0aWwvaXNOb3ROdWxsT3JVbmRlZmluZWQnO1xuXG5cbmV4cG9ydCBpbnRlcmZhY2UgU3RpY2t5UG9zaXRpb25zIHtcbiAgb2Zmc2V0WTogbnVtYmVyO1xuICBib3R0b21Cb3VuZGFyeTogbnVtYmVyIHwgbnVsbDtcbiAgdXBwZXJTY3JlZW5FZGdlQXQ/OiBudW1iZXI7XG4gIG1hcmdpblRvcD86IG51bWJlcjtcbiAgbWFyZ2luQm90dG9tPzogbnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFN0aWNreVN0YXR1cyB7XG4gIGlzU3RpY2t5OiBib29sZWFuO1xuICByZWFjaGVkVXBwZXJFZGdlOiBib29sZWFuO1xuICByZWFjaGVkTG93ZXJFZGdlOiBib29sZWFuO1xuICBtYXJnaW5Ub3A/OiBudW1iZXI7XG4gIG1hcmdpbkJvdHRvbT86IG51bWJlcjtcbn1cblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW3N0aWNreVRoaW5nXSdcbn0pXG5leHBvcnQgY2xhc3MgU3RpY2t5VGhpbmdEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB7XG5cbiAgZmlsdGVyR2F0ZSA9IGZhbHNlO1xuICBtYXJnaW5Ub3AkID0gbmV3IEJlaGF2aW9yU3ViamVjdCgwKTtcbiAgbWFyZ2luQm90dG9tJCA9IG5ldyBCZWhhdmlvclN1YmplY3QoMCk7XG4gIGVuYWJsZSQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KHRydWUpO1xuXG4gIEBJbnB1dCgpIHNjcm9sbENvbnRhaW5lcjogc3RyaW5nIHwgSFRNTEVsZW1lbnQgfCB1bmRlZmluZWQ7XG4gIEBJbnB1dCgpIGF1ZGl0VGltZSA9IDA7XG4gIEBJbnB1dCgpIHNldCBtYXJnaW5Ub3AodmFsdWU6IG51bWJlcikge1xuICAgIHRoaXMubWFyZ2luVG9wJC5uZXh0KHZhbHVlKTtcbiAgfVxuXG4gIEBJbnB1dCgpIHNldCBtYXJnaW5Cb3R0b20odmFsdWU6IG51bWJlcikge1xuICAgIHRoaXMubWFyZ2luQm90dG9tJC5uZXh0KHZhbHVlKTtcbiAgfVxuXG4gIEBJbnB1dCgpIHNldCBlbmFibGUodmFsdWU6IGJvb2xlYW4pIHtcbiAgICB0aGlzLmVuYWJsZSQubmV4dCh2YWx1ZSk7XG4gIH1cbiAgQElucHV0KCdzcGFjZXInKSBzcGFjZXJFbGVtZW50OiBIVE1MRWxlbWVudCB8IHVuZGVmaW5lZDtcbiAgQElucHV0KCdib3VuZGFyeScpIGJvdW5kYXJ5RWxlbWVudDogSFRNTEVsZW1lbnQgfCB1bmRlZmluZWQ7XG4gIHN0aWNreSA9IGZhbHNlO1xuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmlzLXN0aWNreScpIGlzU3RpY2t5ID0gZmFsc2U7XG4gIEBIb3N0QmluZGluZygnY2xhc3MuYm91bmRhcnktcmVhY2hlZCcpIGJvdW5kYXJ5UmVhY2hlZCA9IGZhbHNlO1xuICBASG9zdEJpbmRpbmcoJ2NsYXNzLnVwcGVyLWJvdW5kLXJlYWNoZWQnKSB1cHBlckJvdW5kUmVhY2hlZCA9IGZhbHNlO1xuICBAT3V0cHV0KCkgc3RpY2t5U3RhdHVzOiBFdmVudEVtaXR0ZXI8U3RpY2t5U3RhdHVzPiA9IG5ldyBFdmVudEVtaXR0ZXI8U3RpY2t5U3RhdHVzPigpO1xuICBAT3V0cHV0KCkgc3RpY2t5UG9zaXRpb246IEV2ZW50RW1pdHRlcjxTdGlja3lQb3NpdGlvbnM+ID0gbmV3IEV2ZW50RW1pdHRlcjxTdGlja3lQb3NpdGlvbnM+KCk7XG5cbiAgLyoqXG4gICAqIFRoZSBmaWVsZCByZXByZXNlbnRzIHNvbWUgcG9zaXRpb24gdmFsdWVzIGluIG5vcm1hbCAobm90IHN0aWNreSkgbW9kZS5cbiAgICogSWYgdGhlIGJyb3dzZXIgc2l6ZSBvciB0aGUgY29udGVudCBvZiB0aGUgcGFnZSBjaGFuZ2VzLCB0aGlzIHZhbHVlIG11c3QgYmUgcmVjYWxjdWxhdGVkLlxuICAgKiAqL1xuICBwcml2YXRlIHNjcm9sbCQgPSBuZXcgU3ViamVjdDxudW1iZXI+KCk7XG4gIHByaXZhdGUgc2Nyb2xsVGhyb3R0bGVkJDogT2JzZXJ2YWJsZTxudW1iZXI+O1xuICBwcml2YXRlIHRhcmdldCA9IHRoaXMuZ2V0U2Nyb2xsVGFyZ2V0KCk7XG5cbiAgcHJpdmF0ZSByZXNpemUkID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcbiAgcHJpdmF0ZSByZXNpemVUaHJvdHRsZWQkOiBPYnNlcnZhYmxlPHZvaWQ+O1xuICBwcml2YXRlIGV4dHJhb3JkaW5hcnlDaGFuZ2UkID0gbmV3IEJlaGF2aW9yU3ViamVjdDx2b2lkPih1bmRlZmluZWQpO1xuXG4gIHByaXZhdGUgc3RhdHVzJDogT2JzZXJ2YWJsZTxTdGlja3lTdGF0dXM+O1xuXG4gIHByaXZhdGUgY29tcG9uZW50RGVzdHJveWVkID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcbiAgcHJpdmF0ZSBlbGVtZW50T2Zmc2V0WTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN0aWNreUVsZW1lbnQ6IEVsZW1lbnRSZWYsIEBJbmplY3QoUExBVEZPUk1fSUQpIHByaXZhdGUgcGxhdGZvcm1JZDogc3RyaW5nKSB7XG5cbiAgICAvKipcbiAgICAgKiBUaHJvdHRsZSB0aGUgc2Nyb2xsIHRvIGFuaW1hdGlvbiBmcmFtZSAoYXJvdW5kIDE2LjY3bXMpICovXG4gICAgdGhpcy5zY3JvbGxUaHJvdHRsZWQkID0gdGhpcy5zY3JvbGwkXG4gICAgICAucGlwZShcbiAgICAgICAgdGhyb3R0bGVUaW1lKDAsIGFuaW1hdGlvbkZyYW1lKSxcbiAgICAgICAgc2hhcmUoKVxuICAgICAgKTtcblxuICAgIC8qKlxuICAgICAqIFRocm90dGxlIHRoZSByZXNpemUgdG8gYW5pbWF0aW9uIGZyYW1lIChhcm91bmQgMTYuNjdtcykgKi9cbiAgICB0aGlzLnJlc2l6ZVRocm90dGxlZCQgPSB0aGlzLnJlc2l6ZSRcbiAgICAgIC5waXBlKFxuICAgICAgICB0aHJvdHRsZVRpbWUoMCwgYW5pbWF0aW9uRnJhbWUpLFxuICAgICAgICAvLyBlbWl0IG9uY2Ugc2luY2Ugd2UgYXJlIGN1cnJlbnRseSB1c2luZyBjb21iaW5lTGF0ZXN0XG4gICAgICAgIHN0YXJ0V2l0aChudWxsKSxcbiAgICAgICAgc2hhcmUoKVxuICAgICAgKTtcblxuXG4gICAgdGhpcy5zdGF0dXMkID0gY29tYmluZUxhdGVzdChcbiAgICAgIHRoaXMuZW5hYmxlJCxcbiAgICAgIHRoaXMuc2Nyb2xsVGhyb3R0bGVkJCxcbiAgICAgIHRoaXMubWFyZ2luVG9wJCxcbiAgICAgIHRoaXMubWFyZ2luQm90dG9tJCxcbiAgICAgIHRoaXMuZXh0cmFvcmRpbmFyeUNoYW5nZSQsXG4gICAgICB0aGlzLnJlc2l6ZVRocm90dGxlZCQsXG4gICAgKVxuICAgICAgLnBpcGUoXG4gICAgICAgIGZpbHRlcigoW2VuYWJsZWRdKSA9PiB0aGlzLmNoZWNrRW5hYmxlZChlbmFibGVkKSksXG4gICAgICAgIG1hcCgoW2VuYWJsZWQsIHBhZ2VZT2Zmc2V0LCBtYXJnaW5Ub3AsIG1hcmdpbkJvdHRvbV0pID0+IHRoaXMuZGV0ZXJtaW5lU3RhdHVzKHRoaXMuZGV0ZXJtaW5lRWxlbWVudE9mZnNldHMoKSwgcGFnZVlPZmZzZXQsIG1hcmdpblRvcCwgbWFyZ2luQm90dG9tLCBlbmFibGVkKSksXG4gICAgICAgIHNoYXJlKCksXG4gICAgICApO1xuXG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgY29uc3Qgb3BlcmF0b3JzID0gdGhpcy5zY3JvbGxDb250YWluZXIgP1xuICAgICAgcGlwZSh0YWtlVW50aWwodGhpcy5jb21wb25lbnREZXN0cm95ZWQpKSA6XG4gICAgICBwaXBlKGF1ZGl0VGltZSh0aGlzLmF1ZGl0VGltZSksIHRha2VVbnRpbCh0aGlzLmNvbXBvbmVudERlc3Ryb3llZCkpO1xuICAgIHRoaXMuc3RhdHVzJFxuICAgICAgLnBpcGUob3BlcmF0b3JzKVxuICAgICAgLnN1YnNjcmliZSgoc3RhdHVzOiBTdGlja3lTdGF0dXMpID0+IHtcbiAgICAgICAgdGhpcy5zZXRTdGlja3koc3RhdHVzKTtcbiAgICAgICAgdGhpcy5zZXRTdGF0dXMoc3RhdHVzKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHJlY2FsY3VsYXRlKCk6IHZvaWQge1xuICAgIGlmIChpc1BsYXRmb3JtQnJvd3Nlcih0aGlzLnBsYXRmb3JtSWQpKSB7XG4gICAgICAvLyBNYWtlIHN1cmUgdG8gYmUgaW4gdGhlIG5leHQgdGljayBieSB1c2luZyB0aW1lb3V0XG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgdGhpcy5leHRyYW9yZGluYXJ5Q2hhbmdlJC5uZXh0KHVuZGVmaW5lZCk7XG4gICAgICB9LCAwKTtcbiAgICB9XG4gIH1cblxuXG4gIC8qKlxuICAgKiBUaGlzIGlzIG5hc3R5IGNvZGUgdGhhdCBzaG91bGQgYmUgcmVmYWN0b3JlZCBhdCBzb21lIHBvaW50LlxuICAgKlxuICAgKiBUaGUgUHJvYmxlbSBpcywgd2UgZmlsdGVyIGZvciBlbmFibGVkLiBTbyB0aGF0IHRoZSBjb2RlIGRvZXNuJ3QgcnVuXG4gICAqIGlmIEBJbnB1dCBlbmFibGVkID0gZmFsc2UuIEJ1dCBpZiB0aGUgdXNlciBkaXNhYmxlcywgd2UgbmVlZCBleGFjdGx5IDFcbiAgICogZW1pdCBpbiBvcmRlciB0byByZXNldCBhbmQgY2FsbCByZW1vdmVTdGlja3kuIFNvIHRoaXMgbWV0aG9kIGJhc2ljYWxseVxuICAgKiB0dXJucyB0aGUgZmlsdGVyIGluIFwiZmlsdGVyLCBidXQgbGV0IHRoZSBmaXJzdCBwYXNzXCIuXG4gICAqICovXG4gIGNoZWNrRW5hYmxlZChlbmFibGVkOiBib29sZWFuKTogYm9vbGVhbiB7XG5cbiAgICBpZiAoIWlzUGxhdGZvcm1Ccm93c2VyKHRoaXMucGxhdGZvcm1JZCkpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBpZiAoZW5hYmxlZCkge1xuICAgICAgLy8gcmVzZXQgdGhlIGdhdGVcbiAgICAgIHRoaXMuZmlsdGVyR2F0ZSA9IGZhbHNlO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICh0aGlzLmZpbHRlckdhdGUpIHtcbiAgICAgICAgLy8gZ2F0ZSBjbG9zZWQsIGZpcnN0IGVtaXQgaGFzIGhhcHBlbmVkXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIHRoaXMgaXMgdGhlIGZpcnN0IGVtaXQgZm9yIGVuYWJsZWQgPSBmYWxzZSxcbiAgICAgICAgLy8gbGV0IGl0IHBhc3MsIGFuZCBhY3RpdmF0ZSB0aGUgZ2F0ZVxuICAgICAgICAvLyBzbyB0aGUgbmV4dCB3b250IHBhc3MuXG4gICAgICAgIHRoaXMuZmlsdGVyR2F0ZSA9IHRydWU7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgIH1cblxuXG4gIH1cblxuICBASG9zdExpc3RlbmVyKCd3aW5kb3c6cmVzaXplJywgW10pXG4gIG9uV2luZG93UmVzaXplKCk6IHZvaWQge1xuICAgIGlmIChpc1BsYXRmb3JtQnJvd3Nlcih0aGlzLnBsYXRmb3JtSWQpKSB7XG4gICAgICB0aGlzLnJlc2l6ZSQubmV4dCgpO1xuICAgIH1cbiAgfVxuXG4gIHNldHVwTGlzdGVuZXIoKTogdm9pZCB7XG4gICAgaWYgKGlzUGxhdGZvcm1Ccm93c2VyKHRoaXMucGxhdGZvcm1JZCkpIHtcbiAgICAgIGNvbnN0IHRhcmdldCA9IHRoaXMuZ2V0U2Nyb2xsVGFyZ2V0KCk7XG4gICAgICB0YXJnZXQuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5saXN0ZW5lcik7XG4gICAgfVxuICB9XG5cbiAgbGlzdGVuZXIgPSAoZTogRXZlbnQpID0+IHtcbiAgICBjb25zdCB1cHBlclNjcmVlbkVkZ2VBdCA9IChlLnRhcmdldCBhcyBIVE1MRWxlbWVudCkuc2Nyb2xsVG9wIHx8IHdpbmRvdy5wYWdlWU9mZnNldDtcbiAgICB0aGlzLnNjcm9sbCQubmV4dCh1cHBlclNjcmVlbkVkZ2VBdCk7XG4gIH1cblxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuY2hlY2tTZXR1cCgpO1xuICAgIHRoaXMuc2V0dXBMaXN0ZW5lcigpO1xuICAgIHRoaXMuZWxlbWVudE9mZnNldFkgPSB0aGlzLmRldGVybWluZUVsZW1lbnRPZmZzZXRzKCkub2Zmc2V0WTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIHRoaXMudGFyZ2V0LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMubGlzdGVuZXIpO1xuICAgIHRoaXMuY29tcG9uZW50RGVzdHJveWVkLm5leHQoKTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0U2Nyb2xsVGFyZ2V0KCk6IEVsZW1lbnQgfCBXaW5kb3cge1xuICAgIGxldCB0YXJnZXQ6IEVsZW1lbnQgfCBXaW5kb3c7XG4gICAgaWYgKHRoaXMuc2Nyb2xsQ29udGFpbmVyICYmIHR5cGVvZiB0aGlzLnNjcm9sbENvbnRhaW5lciA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGhpcy5zY3JvbGxDb250YWluZXIpO1xuICAgICAgdGhpcy5tYXJnaW5Ub3AkLm5leHQoSW5maW5pdHkpO1xuICAgICAgdGhpcy5hdWRpdFRpbWUgPSAwO1xuICAgIH0gZWxzZSBpZiAodGhpcy5zY3JvbGxDb250YWluZXIgJiYgdGhpcy5zY3JvbGxDb250YWluZXIgaW5zdGFuY2VvZiBIVE1MRWxlbWVudCkge1xuICAgICAgdGFyZ2V0ID0gdGhpcy5zY3JvbGxDb250YWluZXI7XG4gICAgICB0aGlzLm1hcmdpblRvcCQubmV4dChJbmZpbml0eSk7XG4gICAgICB0aGlzLmF1ZGl0VGltZSA9IDA7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRhcmdldCA9IHdpbmRvdztcbiAgICB9XG4gICAgcmV0dXJuIHRhcmdldDtcbiAgfVxuICBnZXRDb21wdXRlZFN0eWxlKGVsOiBIVE1MRWxlbWVudCk6IENsaWVudFJlY3QgfCBET01SZWN0IHtcbiAgICByZXR1cm4gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gIH1cblxuICBwcml2YXRlIGRldGVybWluZVN0YXR1cyhvcmlnaW5hbFZhbHM6IFN0aWNreVBvc2l0aW9ucywgcGFnZVlPZmZzZXQ6IG51bWJlciwgbWFyZ2luVG9wOiBudW1iZXIsIG1hcmdpbkJvdHRvbTogbnVtYmVyLCBlbmFibGVkOiBib29sZWFuKSB7XG4gICAgY29uc3QgZWxlbWVudFBvcyA9IHRoaXMuZGV0ZXJtaW5lRWxlbWVudE9mZnNldHMoKTtcbiAgICBsZXQgaXNTdGlja3kgPSBlbmFibGVkICYmIHBhZ2VZT2Zmc2V0ID4gb3JpZ2luYWxWYWxzLm9mZnNldFk7XG4gICAgaWYgKHBhZ2VZT2Zmc2V0IDwgdGhpcy5lbGVtZW50T2Zmc2V0WSkge1xuICAgICAgaXNTdGlja3kgPSBmYWxzZTtcbiAgICB9XG4gICAgY29uc3Qgc3RpY2t5RWxlbWVudEhlaWdodCA9IHRoaXMuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudCkuaGVpZ2h0O1xuICAgIC8vIGNvbnN0IHJlYWNoZWRMb3dlckVkZ2UgPSBpc05vdE51bGxPclVuZGVmaW5lZCh0aGlzLmJvdW5kYXJ5RWxlbWVudCkgPyB0aGlzLmJvdW5kYXJ5RWxlbWVudCAmJiB3aW5kb3cucGFnZVlPZmZzZXQgKyBzdGlja3lFbGVtZW50SGVpZ2h0ICsgbWFyZ2luQm90dG9tID49IChvcmlnaW5hbFZhbHMuYm90dG9tQm91bmRhcnkgLSBtYXJnaW5Ub3AgKiAxLjApIDogdW5kZWZpbmVkO1xuICAgIC8vIGNvbnN0IHJlYWNoZWRVcHBlckVkZ2UgPSBpc05vdE51bGxPclVuZGVmaW5lZCh0aGlzLmJvdW5kYXJ5RWxlbWVudCkgPyB3aW5kb3cucGFnZVlPZmZzZXQgPCAodGhpcy5ib3VuZGFyeUVsZW1lbnQub2Zmc2V0VG9wICsgbWFyZ2luVG9wICogMS4wKSA6IHVuZGVmaW5lZDtcblx0ICBjb25zdCByZWFjaGVkTG93ZXJFZGdlID0gKHRoaXMuYm91bmRhcnlFbGVtZW50IT1udWxsKSA/IHRoaXMuYm91bmRhcnlFbGVtZW50ICYmIHdpbmRvdy5wYWdlWU9mZnNldCArIHN0aWNreUVsZW1lbnRIZWlnaHQgKyBtYXJnaW5Cb3R0b20gPj0gKG9yaWdpbmFsVmFscy5ib3R0b21Cb3VuZGFyeSAtIG1hcmdpblRvcCAqIDEuMCkgOiB1bmRlZmluZWQ7XG4gICAgY29uc3QgcmVhY2hlZFVwcGVyRWRnZSA9ICh0aGlzLmJvdW5kYXJ5RWxlbWVudCE9bnVsbCkgPyB3aW5kb3cucGFnZVlPZmZzZXQgPCAodGhpcy5ib3VuZGFyeUVsZW1lbnQub2Zmc2V0VG9wICsgbWFyZ2luVG9wICogMS4wKSA6IHVuZGVmaW5lZDtcbiAgICB0aGlzLnN0aWNreVBvc2l0aW9uLmVtaXQoey4uLmVsZW1lbnRQb3MsIHVwcGVyU2NyZWVuRWRnZUF0OiBwYWdlWU9mZnNldCwgbWFyZ2luQm90dG9tLCBtYXJnaW5Ub3B9KTtcbiAgICByZXR1cm4ge1xuICAgICAgaXNTdGlja3ksXG4gICAgICByZWFjaGVkVXBwZXJFZGdlLFxuICAgICAgcmVhY2hlZExvd2VyRWRnZSxcbiAgICB9O1xuXG4gIH1cblxuXG4gIC8vIG5vdCBhbHdheXMgcGl4ZWwuIGUuZy4gaWU5XG4gIHByaXZhdGUgZ2V0TWFyZ2lucygpOiB7IHRvcDogbnVtYmVyLCBib3R0b206IG51bWJlciB9IHtcbiAgICBjb25zdCBzdGlja3lTdHlsZXMgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudCk7XG4gICAgY29uc3QgdG9wID0gcGFyc2VJbnQoc3RpY2t5U3R5bGVzLm1hcmdpblRvcCwgMTApO1xuICAgIGNvbnN0IGJvdHRvbSA9IHBhcnNlSW50KHN0aWNreVN0eWxlcy5tYXJnaW5Cb3R0b20sIDEwKTtcbiAgICByZXR1cm4ge3RvcCwgYm90dG9tfTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBvZmZzZXQgZm9yIGVsZW1lbnQuIElmIHRoZSBlbGVtZW50XG4gICAqIGN1cnJlbnRseSBpcyBzdGlja3ksIGl0IHdpbGwgZ2V0IHJlbW92ZWRcbiAgICogdG8gYWNjZXNzIHRoZSBvcmlnaW5hbCBwb3NpdGlvbi4gT3RoZXJcbiAgICogd2lzZSB0aGlzIHdvdWxkIGp1c3QgYmUgMCBmb3IgZml4ZWQgZWxlbWVudHMuICovXG4gIHByaXZhdGUgZGV0ZXJtaW5lRWxlbWVudE9mZnNldHMoKTogU3RpY2t5UG9zaXRpb25zIHtcbiAgICBpZiAodGhpcy5zdGlja3kpIHtcbiAgICAgIHRoaXMucmVtb3ZlU3RpY2t5KCk7XG4gICAgfVxuICAgIGxldCBib3R0b21Cb3VuZGFyeTogbnVtYmVyIHwgbnVsbCA9IG51bGw7XG5cbiAgICBpZiAodGhpcy5ib3VuZGFyeUVsZW1lbnQpIHtcbiAgICAgIGNvbnN0IGJvdW5kYXJ5RWxlbWVudEhlaWdodCA9IHRoaXMuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLmJvdW5kYXJ5RWxlbWVudCkuaGVpZ2h0O1xuICAgICAgY29uc3QgYm91bmRhcnlFbGVtZW50T2Zmc2V0ID0gZ2V0UG9zaXRpb24odGhpcy5ib3VuZGFyeUVsZW1lbnQpLnk7XG4gICAgICBib3R0b21Cb3VuZGFyeSA9IGJvdW5kYXJ5RWxlbWVudEhlaWdodCArIGJvdW5kYXJ5RWxlbWVudE9mZnNldDtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgIG9mZnNldFk6IChnZXRQb3NpdGlvbih0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudCkueSAtIHRoaXMubWFyZ2luVG9wJC52YWx1ZSksIGJvdHRvbUJvdW5kYXJ5XG4gICAgfTtcbiAgfVxuXG4gIHByaXZhdGUgbWFrZVN0aWNreShib3VuZGFyeVJlYWNoZWQ6IGJvb2xlYW4gPSBmYWxzZSwgbWFyZ2luVG9wOiBudW1iZXIsIG1hcmdpbkJvdHRvbTogbnVtYmVyKTogdm9pZCB7XG4gICAgLy8gZG8gdGhpcyBiZWZvcmUgc2V0dGluZyBpdCB0byBwb3M6Zml4ZWRcbiAgICBjb25zdCB7d2lkdGgsIGhlaWdodCwgbGVmdH0gPSB0aGlzLmdldENvbXB1dGVkU3R5bGUodGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQpO1xuICAgIGNvbnN0IG9mZlNldCA9IGJvdW5kYXJ5UmVhY2hlZCA/ICh0aGlzLmdldENvbXB1dGVkU3R5bGUodGhpcy5ib3VuZGFyeUVsZW1lbnQpLmJvdHRvbSAtIGhlaWdodCAtIHRoaXMubWFyZ2luQm90dG9tJC52YWx1ZSkgOiB0aGlzLm1hcmdpblRvcCQudmFsdWU7XG4gICAgaWYgKHRoaXMuc2Nyb2xsQ29udGFpbmVyICYmICF0aGlzLnN0aWNreSkge1xuICAgICAgdGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc3R5bGUucG9zaXRpb24gPSAnc3RpY2t5JztcbiAgICAgIHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50LnN0eWxlLnRvcCA9ICcwcHgnO1xuICAgICAgdGhpcy5zdGlja3kgPSB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS5wb3NpdGlvbiA9ICdmaXhlZCc7XG4gICAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS50b3AgPSBvZmZTZXQgKyAncHgnO1xuICAgICAgdGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc3R5bGUubGVmdCA9IGxlZnQgKyAncHgnO1xuICAgICAgdGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc3R5bGUud2lkdGggPSBgJHt3aWR0aH1weGA7XG4gICAgfVxuICAgIGlmICh0aGlzLnNwYWNlckVsZW1lbnQpIHtcbiAgICAgIGNvbnN0IHNwYWNlckhlaWdodCA9IG1hcmdpbkJvdHRvbSArIGhlaWdodCArIG1hcmdpblRvcDtcbiAgICAgIHRoaXMuc3BhY2VyRWxlbWVudC5zdHlsZS5oZWlnaHQgPSBgJHtzcGFjZXJIZWlnaHR9cHhgO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgZGV0ZXJtaW5lQm91bmRhcnlSZWFjaGVkKGJvdW5kYXJ5SGVpZ2h0OiBudW1iZXIsIHN0aWNreUVsSGVpZ2h0OiBudW1iZXIsIGNzc01hcmdpbnMsIG1hcmdpblRvcDogbnVtYmVyLCBtYXJnaW5Cb3R0b206IG51bWJlciwgdXBwZXJTY3JlZW5FZGdlQXQ6IG51bWJlcikge1xuXG4gICAgY29uc3QgYm91bmRhcnlFbGVtZW50UG9zID0gZ2V0UG9zaXRpb24odGhpcy5ib3VuZGFyeUVsZW1lbnQpO1xuXG4gICAgY29uc3QgYm91bmRhcnlFbGVtZW50TG93ZXJFZGdlID0gYm91bmRhcnlFbGVtZW50UG9zLnkgKyBib3VuZGFyeUhlaWdodDtcbiAgICBjb25zdCBsb3dlckVkZ2VTdGlja3lFbGVtZW50ID0gdXBwZXJTY3JlZW5FZGdlQXQgKyBzdGlja3lFbEhlaWdodCArIG1hcmdpblRvcCArIGNzc01hcmdpbnMudG9wICsgbWFyZ2luQm90dG9tICsgY3NzTWFyZ2lucy5ib3R0b207XG5cbiAgICByZXR1cm4gYm91bmRhcnlFbGVtZW50TG93ZXJFZGdlIDw9IGxvd2VyRWRnZVN0aWNreUVsZW1lbnQ7XG4gIH1cblxuICBwcml2YXRlIGNoZWNrU2V0dXAoKSB7XG4gICAgaWYgKGlzRGV2TW9kZSgpICYmICF0aGlzLnNwYWNlckVsZW1lbnQpIHtcbiAgICAgIGNvbnNvbGUud2FybihgKioqKioqVGhlcmUgbWlnaHQgYmUgYW4gaXNzdWUgd2l0aCB5b3VyIHN0aWNreSBkaXJlY3RpdmUhKioqKioqXG5cbllvdSBoYXZlbid0IHNwZWNpZmllZCBhIHNwYWNlciBlbGVtZW50LiBUaGlzIHdpbGwgY2F1c2UgdGhlIHBhZ2UgdG8ganVtcC5cblxuQmVzdCBwcmFjdGlzZSBpcyB0byBwcm92aWRlIGEgc3BhY2VyIGVsZW1lbnQgKGUuZy4gYSBkaXYpIHJpZ2h0IGJlZm9yZS9hZnRlciB0aGUgc3RpY2t5IGVsZW1lbnQuXG5UaGVuIHBhc3MgdGhlIHNwYWNlciBlbGVtZW50IGFzIGlucHV0OlxuXG48ZGl2ICNzcGFjZXI+PC9kaXY+XG5cbjxkaXYgc3RpY2t5VGhpbmc9XCJcIiBbc3BhY2VyXT1cInNwYWNlclwiPlxuICAgIEkgYW0gc3RpY2t5IVxuPC9kaXY+YCk7XG4gICAgfVxuICB9XG5cblxuICBwcml2YXRlIHNldFN0aWNreShzdGF0dXM6IFN0aWNreVN0YXR1cyk6IHZvaWQge1xuICAgIGlmIChzdGF0dXMuaXNTdGlja3kpIHtcbiAgICAgIGlmICh0aGlzLnVwcGVyQm91bmRSZWFjaGVkKSB7XG4gICAgICAgIHRoaXMucmVtb3ZlU3RpY2t5KCk7XG4gICAgICAgIHRoaXMuaXNTdGlja3kgPSBmYWxzZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMubWFrZVN0aWNreShzdGF0dXMucmVhY2hlZExvd2VyRWRnZSwgc3RhdHVzLm1hcmdpblRvcCwgc3RhdHVzLm1hcmdpbkJvdHRvbSk7XG4gICAgICAgIHRoaXMuaXNTdGlja3kgPSB0cnVlO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJlbW92ZVN0aWNreSgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0U3RhdHVzKHN0YXR1czogU3RpY2t5U3RhdHVzKSB7XG4gICAgdGhpcy51cHBlckJvdW5kUmVhY2hlZCA9IHN0YXR1cy5yZWFjaGVkVXBwZXJFZGdlO1xuICAgIHRoaXMuYm91bmRhcnlSZWFjaGVkID0gc3RhdHVzLnJlYWNoZWRMb3dlckVkZ2U7XG4gICAgdGhpcy5zdGlja3lTdGF0dXMubmV4dChzdGF0dXMpO1xuICB9XG5cbiAgcHJpdmF0ZSByZW1vdmVTdGlja3koKTogdm9pZCB7XG4gICAgdGhpcy5ib3VuZGFyeVJlYWNoZWQgPSBmYWxzZTtcbiAgICB0aGlzLnN0aWNreSA9IGZhbHNlO1xuICAgIHRoaXMuc3RpY2t5RWxlbWVudC5uYXRpdmVFbGVtZW50LnN0eWxlLnBvc2l0aW9uID0gJyc7XG4gICAgdGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc3R5bGUud2lkdGggPSAnYXV0byc7XG4gICAgdGhpcy5zdGlja3lFbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc3R5bGUubGVmdCA9ICdhdXRvJztcbiAgICB0aGlzLnN0aWNreUVsZW1lbnQubmF0aXZlRWxlbWVudC5zdHlsZS50b3AgPSAnYXV0byc7XG4gICAgaWYgKHRoaXMuc3BhY2VyRWxlbWVudCkge1xuICAgICAgdGhpcy5zcGFjZXJFbGVtZW50LnN0eWxlLmhlaWdodCA9ICcwJztcbiAgICB9XG4gIH1cbn1cblxuLy8gVGhhbmtzIHRvIGh0dHBzOi8vc3RhbmtvLmdpdGh1Yi5pby9qYXZhc2NyaXB0LWdldC1lbGVtZW50LW9mZnNldC9cbmZ1bmN0aW9uIGdldFBvc2l0aW9uKGVsKSB7XG4gIGxldCB0b3AgPSAwO1xuICBsZXQgbGVmdCA9IDA7XG4gIGxldCBlbGVtZW50ID0gZWw7XG5cbiAgLy8gTG9vcCB0aHJvdWdoIHRoZSBET00gdHJlZVxuICAvLyBhbmQgYWRkIGl0J3MgcGFyZW50J3Mgb2Zmc2V0IHRvIGdldCBwYWdlIG9mZnNldFxuICBkbyB7XG4gICAgdG9wICs9IGVsZW1lbnQub2Zmc2V0VG9wIHx8IDA7XG4gICAgbGVmdCArPSBlbGVtZW50Lm9mZnNldExlZnQgfHwgMDtcbiAgICBlbGVtZW50ID0gZWxlbWVudC5vZmZzZXRQYXJlbnQ7XG4gIH0gd2hpbGUgKGVsZW1lbnQpO1xuXG4gIHJldHVybiB7XG4gICAgeTogdG9wLFxuICAgIHg6IGxlZnQsXG4gIH07XG59XG4iXX0=