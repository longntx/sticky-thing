/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of angular-sticky-things
 */
export { StickyThingDirective } from './lib/sticky-thing.directive';
export { AngularStickyThingsModule } from './lib/angular-sticky-things.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AdzExay9hbmd1bGFyLXN0aWNreS10aGluZ3MvIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEscUNBQWMsOEJBQThCLENBQUM7QUFDN0MsMENBQWMsb0NBQW9DLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIGFuZ3VsYXItc3RpY2t5LXRoaW5nc1xuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL3N0aWNreS10aGluZy5kaXJlY3RpdmUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvYW5ndWxhci1zdGlja3ktdGhpbmdzLm1vZHVsZSc7XG4iXX0=