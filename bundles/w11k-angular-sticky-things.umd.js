(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('rxjs'), require('rxjs/internal/scheduler/animationFrame'), require('rxjs/operators')) :
    typeof define === 'function' && define.amd ? define('@w11k/angular-sticky-things', ['exports', '@angular/core', '@angular/common', 'rxjs', 'rxjs/internal/scheduler/animationFrame', 'rxjs/operators'], factory) :
    (global = global || self, factory((global.w11k = global.w11k || {}, global.w11k['angular-sticky-things'] = {}), global.ng.core, global.ng.common, global.rxjs, global.animationFrame, global.rxjs.operators));
}(this, (function (exports, core, common, rxjs, animationFrame, operators) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function StickyPositions() { }
    if (false) {
        /** @type {?} */
        StickyPositions.prototype.offsetY;
        /** @type {?} */
        StickyPositions.prototype.bottomBoundary;
        /** @type {?|undefined} */
        StickyPositions.prototype.upperScreenEdgeAt;
        /** @type {?|undefined} */
        StickyPositions.prototype.marginTop;
        /** @type {?|undefined} */
        StickyPositions.prototype.marginBottom;
    }
    /**
     * @record
     */
    function StickyStatus() { }
    if (false) {
        /** @type {?} */
        StickyStatus.prototype.isSticky;
        /** @type {?} */
        StickyStatus.prototype.reachedUpperEdge;
        /** @type {?} */
        StickyStatus.prototype.reachedLowerEdge;
        /** @type {?|undefined} */
        StickyStatus.prototype.marginTop;
        /** @type {?|undefined} */
        StickyStatus.prototype.marginBottom;
    }
    var StickyThingDirective = /** @class */ (function () {
        function StickyThingDirective(stickyElement, platformId) {
            var _this = this;
            this.stickyElement = stickyElement;
            this.platformId = platformId;
            this.filterGate = false;
            this.marginTop$ = new rxjs.BehaviorSubject(0);
            this.marginBottom$ = new rxjs.BehaviorSubject(0);
            this.enable$ = new rxjs.BehaviorSubject(true);
            this.auditTime = 0;
            this.sticky = false;
            this.isSticky = false;
            this.boundaryReached = false;
            this.upperBoundReached = false;
            this.stickyStatus = new core.EventEmitter();
            this.stickyPosition = new core.EventEmitter();
            /**
             * The field represents some position values in normal (not sticky) mode.
             * If the browser size or the content of the page changes, this value must be recalculated.
             *
             */
            this.scroll$ = new rxjs.Subject();
            this.target = this.getScrollTarget();
            this.resize$ = new rxjs.Subject();
            this.extraordinaryChange$ = new rxjs.BehaviorSubject(undefined);
            this.componentDestroyed = new rxjs.Subject();
            this.listener = (/**
             * @param {?} e
             * @return {?}
             */
            function (e) {
                /** @type {?} */
                var upperScreenEdgeAt = ((/** @type {?} */ (e.target))).scrollTop || window.pageYOffset;
                _this.scroll$.next(upperScreenEdgeAt);
            });
            /**
             * Throttle the scroll to animation frame (around 16.67ms) */
            this.scrollThrottled$ = this.scroll$
                .pipe(operators.throttleTime(0, animationFrame.animationFrame), operators.share());
            /**
             * Throttle the resize to animation frame (around 16.67ms) */
            this.resizeThrottled$ = this.resize$
                .pipe(operators.throttleTime(0, animationFrame.animationFrame), 
            // emit once since we are currently using combineLatest
            operators.startWith(null), operators.share());
            this.status$ = rxjs.combineLatest(this.enable$, this.scrollThrottled$, this.marginTop$, this.marginBottom$, this.extraordinaryChange$, this.resizeThrottled$)
                .pipe(operators.filter((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var _b = __read(_a, 1), enabled = _b[0];
                return _this.checkEnabled(enabled);
            })), operators.map((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var _b = __read(_a, 4), enabled = _b[0], pageYOffset = _b[1], marginTop = _b[2], marginBottom = _b[3];
                return _this.determineStatus(_this.determineElementOffsets(), pageYOffset, marginTop, marginBottom, enabled);
            })), operators.share());
        }
        Object.defineProperty(StickyThingDirective.prototype, "marginTop", {
            set: /**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this.marginTop$.next(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StickyThingDirective.prototype, "marginBottom", {
            set: /**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this.marginBottom$.next(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StickyThingDirective.prototype, "enable", {
            set: /**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this.enable$.next(value);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        StickyThingDirective.prototype.ngAfterViewInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var operators$1 = this.scrollContainer ?
                rxjs.pipe(operators.takeUntil(this.componentDestroyed)) :
                rxjs.pipe(operators.auditTime(this.auditTime), operators.takeUntil(this.componentDestroyed));
            this.status$
                .pipe(operators$1)
                .subscribe((/**
             * @param {?} status
             * @return {?}
             */
            function (status) {
                _this.setSticky(status);
                _this.setStatus(status);
            }));
        };
        /**
         * @return {?}
         */
        StickyThingDirective.prototype.recalculate = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (common.isPlatformBrowser(this.platformId)) {
                // Make sure to be in the next tick by using timeout
                setTimeout((/**
                 * @return {?}
                 */
                function () {
                    _this.extraordinaryChange$.next(undefined);
                }), 0);
            }
        };
        /**
         * This is nasty code that should be refactored at some point.
         *
         * The Problem is, we filter for enabled. So that the code doesn't run
         * if @Input enabled = false. But if the user disables, we need exactly 1
         * emit in order to reset and call removeSticky. So this method basically
         * turns the filter in "filter, but let the first pass".
         * */
        /**
         * This is nasty code that should be refactored at some point.
         *
         * The Problem is, we filter for enabled. So that the code doesn't run
         * if \@Input enabled = false. But if the user disables, we need exactly 1
         * emit in order to reset and call removeSticky. So this method basically
         * turns the filter in "filter, but let the first pass".
         *
         * @param {?} enabled
         * @return {?}
         */
        StickyThingDirective.prototype.checkEnabled = /**
         * This is nasty code that should be refactored at some point.
         *
         * The Problem is, we filter for enabled. So that the code doesn't run
         * if \@Input enabled = false. But if the user disables, we need exactly 1
         * emit in order to reset and call removeSticky. So this method basically
         * turns the filter in "filter, but let the first pass".
         *
         * @param {?} enabled
         * @return {?}
         */
        function (enabled) {
            if (!common.isPlatformBrowser(this.platformId)) {
                return false;
            }
            if (enabled) {
                // reset the gate
                this.filterGate = false;
                return true;
            }
            else {
                if (this.filterGate) {
                    // gate closed, first emit has happened
                    return false;
                }
                else {
                    // this is the first emit for enabled = false,
                    // let it pass, and activate the gate
                    // so the next wont pass.
                    this.filterGate = true;
                    return true;
                }
            }
        };
        /**
         * @return {?}
         */
        StickyThingDirective.prototype.onWindowResize = /**
         * @return {?}
         */
        function () {
            if (common.isPlatformBrowser(this.platformId)) {
                this.resize$.next();
            }
        };
        /**
         * @return {?}
         */
        StickyThingDirective.prototype.setupListener = /**
         * @return {?}
         */
        function () {
            if (common.isPlatformBrowser(this.platformId)) {
                /** @type {?} */
                var target = this.getScrollTarget();
                target.addEventListener('scroll', this.listener);
            }
        };
        /**
         * @return {?}
         */
        StickyThingDirective.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.checkSetup();
            this.setupListener();
            this.elementOffsetY = this.determineElementOffsets().offsetY;
        };
        /**
         * @return {?}
         */
        StickyThingDirective.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.target.removeEventListener('scroll', this.listener);
            this.componentDestroyed.next();
        };
        /**
         * @private
         * @return {?}
         */
        StickyThingDirective.prototype.getScrollTarget = /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var target;
            if (this.scrollContainer && typeof this.scrollContainer === 'string') {
                target = document.querySelector(this.scrollContainer);
                this.marginTop$.next(Infinity);
                this.auditTime = 0;
            }
            else if (this.scrollContainer && this.scrollContainer instanceof HTMLElement) {
                target = this.scrollContainer;
                this.marginTop$.next(Infinity);
                this.auditTime = 0;
            }
            else {
                target = window;
            }
            return target;
        };
        /**
         * @param {?} el
         * @return {?}
         */
        StickyThingDirective.prototype.getComputedStyle = /**
         * @param {?} el
         * @return {?}
         */
        function (el) {
            return el.getBoundingClientRect();
        };
        /**
         * @private
         * @param {?} originalVals
         * @param {?} pageYOffset
         * @param {?} marginTop
         * @param {?} marginBottom
         * @param {?} enabled
         * @return {?}
         */
        StickyThingDirective.prototype.determineStatus = /**
         * @private
         * @param {?} originalVals
         * @param {?} pageYOffset
         * @param {?} marginTop
         * @param {?} marginBottom
         * @param {?} enabled
         * @return {?}
         */
        function (originalVals, pageYOffset, marginTop, marginBottom, enabled) {
            /** @type {?} */
            var elementPos = this.determineElementOffsets();
            /** @type {?} */
            var isSticky = enabled && pageYOffset > originalVals.offsetY;
            if (pageYOffset < this.elementOffsetY) {
                isSticky = false;
            }
            /** @type {?} */
            var stickyElementHeight = this.getComputedStyle(this.stickyElement.nativeElement).height;
            // const reachedLowerEdge = isNotNullOrUndefined(this.boundaryElement) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
            // const reachedUpperEdge = isNotNullOrUndefined(this.boundaryElement) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
            /** @type {?} */
            var reachedLowerEdge = (this.boundaryElement != null) ? this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop * 1.0) : undefined;
            /** @type {?} */
            var reachedUpperEdge = (this.boundaryElement != null) ? window.pageYOffset < (this.boundaryElement.offsetTop + marginTop * 1.0) : undefined;
            this.stickyPosition.emit(__assign({}, elementPos, { upperScreenEdgeAt: pageYOffset, marginBottom: marginBottom, marginTop: marginTop }));
            return {
                isSticky: isSticky,
                reachedUpperEdge: reachedUpperEdge,
                reachedLowerEdge: reachedLowerEdge,
            };
        };
        // not always pixel. e.g. ie9
        // not always pixel. e.g. ie9
        /**
         * @private
         * @return {?}
         */
        StickyThingDirective.prototype.getMargins = 
        // not always pixel. e.g. ie9
        /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var stickyStyles = window.getComputedStyle(this.stickyElement.nativeElement);
            /** @type {?} */
            var top = parseInt(stickyStyles.marginTop, 10);
            /** @type {?} */
            var bottom = parseInt(stickyStyles.marginBottom, 10);
            return { top: top, bottom: bottom };
        };
        /**
         * Gets the offset for element. If the element
         * currently is sticky, it will get removed
         * to access the original position. Other
         * wise this would just be 0 for fixed elements. */
        /**
         * Gets the offset for element. If the element
         * currently is sticky, it will get removed
         * to access the original position. Other
         * wise this would just be 0 for fixed elements.
         * @private
         * @return {?}
         */
        StickyThingDirective.prototype.determineElementOffsets = /**
         * Gets the offset for element. If the element
         * currently is sticky, it will get removed
         * to access the original position. Other
         * wise this would just be 0 for fixed elements.
         * @private
         * @return {?}
         */
        function () {
            if (this.sticky) {
                this.removeSticky();
            }
            /** @type {?} */
            var bottomBoundary = null;
            if (this.boundaryElement) {
                /** @type {?} */
                var boundaryElementHeight = this.getComputedStyle(this.boundaryElement).height;
                /** @type {?} */
                var boundaryElementOffset = getPosition(this.boundaryElement).y;
                bottomBoundary = boundaryElementHeight + boundaryElementOffset;
            }
            return {
                offsetY: (getPosition(this.stickyElement.nativeElement).y - this.marginTop$.value), bottomBoundary: bottomBoundary
            };
        };
        /**
         * @private
         * @param {?=} boundaryReached
         * @param {?=} marginTop
         * @param {?=} marginBottom
         * @return {?}
         */
        StickyThingDirective.prototype.makeSticky = /**
         * @private
         * @param {?=} boundaryReached
         * @param {?=} marginTop
         * @param {?=} marginBottom
         * @return {?}
         */
        function (boundaryReached, marginTop, marginBottom) {
            if (boundaryReached === void 0) { boundaryReached = false; }
            // do this before setting it to pos:fixed
            var _a = this.getComputedStyle(this.stickyElement.nativeElement), width = _a.width, height = _a.height, left = _a.left;
            /** @type {?} */
            var offSet = boundaryReached ? (this.getComputedStyle(this.boundaryElement).bottom - height - this.marginBottom$.value) : this.marginTop$.value;
            if (this.scrollContainer && !this.sticky) {
                this.stickyElement.nativeElement.style.position = 'sticky';
                this.stickyElement.nativeElement.style.top = '0px';
                this.sticky = true;
            }
            else {
                this.stickyElement.nativeElement.style.position = 'fixed';
                this.stickyElement.nativeElement.style.top = offSet + 'px';
                this.stickyElement.nativeElement.style.left = left + 'px';
                this.stickyElement.nativeElement.style.width = width + "px";
            }
            if (this.spacerElement) {
                /** @type {?} */
                var spacerHeight = marginBottom + height + marginTop;
                this.spacerElement.style.height = spacerHeight + "px";
            }
        };
        /**
         * @private
         * @param {?} boundaryHeight
         * @param {?} stickyElHeight
         * @param {?} cssMargins
         * @param {?} marginTop
         * @param {?} marginBottom
         * @param {?} upperScreenEdgeAt
         * @return {?}
         */
        StickyThingDirective.prototype.determineBoundaryReached = /**
         * @private
         * @param {?} boundaryHeight
         * @param {?} stickyElHeight
         * @param {?} cssMargins
         * @param {?} marginTop
         * @param {?} marginBottom
         * @param {?} upperScreenEdgeAt
         * @return {?}
         */
        function (boundaryHeight, stickyElHeight, cssMargins, marginTop, marginBottom, upperScreenEdgeAt) {
            /** @type {?} */
            var boundaryElementPos = getPosition(this.boundaryElement);
            /** @type {?} */
            var boundaryElementLowerEdge = boundaryElementPos.y + boundaryHeight;
            /** @type {?} */
            var lowerEdgeStickyElement = upperScreenEdgeAt + stickyElHeight + marginTop + cssMargins.top + marginBottom + cssMargins.bottom;
            return boundaryElementLowerEdge <= lowerEdgeStickyElement;
        };
        /**
         * @private
         * @return {?}
         */
        StickyThingDirective.prototype.checkSetup = /**
         * @private
         * @return {?}
         */
        function () {
            if (core.isDevMode() && !this.spacerElement) {
                console.warn("******There might be an issue with your sticky directive!******\n\nYou haven't specified a spacer element. This will cause the page to jump.\n\nBest practise is to provide a spacer element (e.g. a div) right before/after the sticky element.\nThen pass the spacer element as input:\n\n<div #spacer></div>\n\n<div stickyThing=\"\" [spacer]=\"spacer\">\n    I am sticky!\n</div>");
            }
        };
        /**
         * @private
         * @param {?} status
         * @return {?}
         */
        StickyThingDirective.prototype.setSticky = /**
         * @private
         * @param {?} status
         * @return {?}
         */
        function (status) {
            if (status.isSticky) {
                if (this.upperBoundReached) {
                    this.removeSticky();
                    this.isSticky = false;
                }
                else {
                    this.makeSticky(status.reachedLowerEdge, status.marginTop, status.marginBottom);
                    this.isSticky = true;
                }
            }
            else {
                this.removeSticky();
            }
        };
        /**
         * @private
         * @param {?} status
         * @return {?}
         */
        StickyThingDirective.prototype.setStatus = /**
         * @private
         * @param {?} status
         * @return {?}
         */
        function (status) {
            this.upperBoundReached = status.reachedUpperEdge;
            this.boundaryReached = status.reachedLowerEdge;
            this.stickyStatus.next(status);
        };
        /**
         * @private
         * @return {?}
         */
        StickyThingDirective.prototype.removeSticky = /**
         * @private
         * @return {?}
         */
        function () {
            this.boundaryReached = false;
            this.sticky = false;
            this.stickyElement.nativeElement.style.position = '';
            this.stickyElement.nativeElement.style.width = 'auto';
            this.stickyElement.nativeElement.style.left = 'auto';
            this.stickyElement.nativeElement.style.top = 'auto';
            if (this.spacerElement) {
                this.spacerElement.style.height = '0';
            }
        };
        StickyThingDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[stickyThing]'
                    },] }
        ];
        /** @nocollapse */
        StickyThingDirective.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: String, decorators: [{ type: core.Inject, args: [core.PLATFORM_ID,] }] }
        ]; };
        StickyThingDirective.propDecorators = {
            scrollContainer: [{ type: core.Input }],
            auditTime: [{ type: core.Input }],
            marginTop: [{ type: core.Input }],
            marginBottom: [{ type: core.Input }],
            enable: [{ type: core.Input }],
            spacerElement: [{ type: core.Input, args: ['spacer',] }],
            boundaryElement: [{ type: core.Input, args: ['boundary',] }],
            isSticky: [{ type: core.HostBinding, args: ['class.is-sticky',] }],
            boundaryReached: [{ type: core.HostBinding, args: ['class.boundary-reached',] }],
            upperBoundReached: [{ type: core.HostBinding, args: ['class.upper-bound-reached',] }],
            stickyStatus: [{ type: core.Output }],
            stickyPosition: [{ type: core.Output }],
            onWindowResize: [{ type: core.HostListener, args: ['window:resize', [],] }]
        };
        return StickyThingDirective;
    }());
    if (false) {
        /** @type {?} */
        StickyThingDirective.prototype.filterGate;
        /** @type {?} */
        StickyThingDirective.prototype.marginTop$;
        /** @type {?} */
        StickyThingDirective.prototype.marginBottom$;
        /** @type {?} */
        StickyThingDirective.prototype.enable$;
        /** @type {?} */
        StickyThingDirective.prototype.scrollContainer;
        /** @type {?} */
        StickyThingDirective.prototype.auditTime;
        /** @type {?} */
        StickyThingDirective.prototype.spacerElement;
        /** @type {?} */
        StickyThingDirective.prototype.boundaryElement;
        /** @type {?} */
        StickyThingDirective.prototype.sticky;
        /** @type {?} */
        StickyThingDirective.prototype.isSticky;
        /** @type {?} */
        StickyThingDirective.prototype.boundaryReached;
        /** @type {?} */
        StickyThingDirective.prototype.upperBoundReached;
        /** @type {?} */
        StickyThingDirective.prototype.stickyStatus;
        /** @type {?} */
        StickyThingDirective.prototype.stickyPosition;
        /**
         * The field represents some position values in normal (not sticky) mode.
         * If the browser size or the content of the page changes, this value must be recalculated.
         *
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.scroll$;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.scrollThrottled$;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.target;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.resize$;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.resizeThrottled$;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.extraordinaryChange$;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.status$;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.componentDestroyed;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.elementOffsetY;
        /** @type {?} */
        StickyThingDirective.prototype.listener;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.stickyElement;
        /**
         * @type {?}
         * @private
         */
        StickyThingDirective.prototype.platformId;
    }
    // Thanks to https://stanko.github.io/javascript-get-element-offset/
    /**
     * @param {?} el
     * @return {?}
     */
    function getPosition(el) {
        /** @type {?} */
        var top = 0;
        /** @type {?} */
        var left = 0;
        /** @type {?} */
        var element = el;
        // Loop through the DOM tree
        // and add it's parent's offset to get page offset
        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);
        return {
            y: top,
            x: left,
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AngularStickyThingsModule = /** @class */ (function () {
        function AngularStickyThingsModule() {
        }
        AngularStickyThingsModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [],
                        declarations: [
                            StickyThingDirective,
                        ],
                        exports: [
                            StickyThingDirective,
                        ]
                    },] }
        ];
        return AngularStickyThingsModule;
    }());

    exports.AngularStickyThingsModule = AngularStickyThingsModule;
    exports.StickyThingDirective = StickyThingDirective;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=w11k-angular-sticky-things.umd.js.map
